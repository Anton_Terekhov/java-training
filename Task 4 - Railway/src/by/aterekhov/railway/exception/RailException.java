package by.aterekhov.railway.exception;

public class RailException extends Exception {

    public RailException()
    {
    }

    public RailException(String message, Throwable exception)
    {
        super(message, exception);
    }

    public RailException(String message)
    {
        super(message);
    }

    public RailException (Throwable exception)
    {
        super(exception);
    }
}
