package by.aterekhov.railway.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import by.aterekhov.railway.train.Train;
import by.aterekhov.railway.train.TrainComparatorByMainElementValue;
import by.aterekhov.railway.train.TrainComparatorByRailElement;
import by.aterekhov.railway.trainpart.RailElement;
import by.aterekhov.railway.trainpart.carriage.sort.RailPassengerCarriage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TrainAction {
    private final static Logger logger = LogManager.getLogger(TrainAction.class);

    public static int calculateTrainPassengers(Train train)
    {
        int passengerAmount = 0;
        for(RailElement element : train.returnAllTrain())
        {
            if (element instanceof RailPassengerCarriage)
            {
                passengerAmount += ((RailPassengerCarriage) element).getPassengerAmount();
            }
        }
        logger.info("Calculation of passengers for train '" + train.getTrainName() +
                "' was finished, amount of passengers = " + passengerAmount + ".");
        return passengerAmount;
    }

    public static int calculateTrainBaggage(Train train)
    {
        int baggageAmount = 0;
        for(RailElement element : train.returnAllTrain())
        {
            if (element instanceof RailPassengerCarriage)
            {
                baggageAmount += ((RailPassengerCarriage) element).getPassengerBaggageAmount();
            }
        }
        logger.info("Calculation of passenger's baggage for train '" + train.getTrainName() +
                "' was finished, amount of baggage = " + baggageAmount + " items.");
        return baggageAmount;
    }

    public static void sortTrain(Train train)
    {
        Comparator<RailElement> comparator = new TrainComparatorByRailElement().thenComparing(new TrainComparatorByMainElementValue());
        train.returnAllTrain().sort(comparator);
        logger.info("Train was sorted. Details are presented below.");
        int i = 1;
        for(RailElement el : train.returnAllTrain())
        {
            logger.info("#" + i + ": " + el.toString());
            i++;
        }
    }

    public static ArrayList<RailPassengerCarriage> searchPassengerCarriage(Train train, int minPassengerAmount, int maxPassengerAmount)
    {
        ArrayList<RailPassengerCarriage> result = new ArrayList<RailPassengerCarriage>();
        for(RailElement el : train.returnAllTrain())
        {
            if(el instanceof RailPassengerCarriage)
            {
                if (minPassengerAmount<= ((RailPassengerCarriage) el).getPassengerAmount() && ((RailPassengerCarriage) el).getPassengerAmount() <= maxPassengerAmount)
                {
                    result.add((RailPassengerCarriage) el);
                }
            }
        }
        logger.info("Search of passenger carriages with passenger amount between " + minPassengerAmount + " and " + maxPassengerAmount + " was finished. Total carriages: " + result.size() + ".");
        if(!result.isEmpty())
        {
            result.sort(new TrainComparatorByMainElementValue().reversed());
            logger.info("Detailed results are:");
            int i = 1;
            for(RailPassengerCarriage carriage : result)
            {
                logger.info("#" + i + ": " + carriage.toString());
                i++;
            }
        }
        return result;
    }
}
