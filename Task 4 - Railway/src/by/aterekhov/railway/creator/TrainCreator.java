package by.aterekhov.railway.creator;

import by.aterekhov.railway.constant.ProjectConstant;
import by.aterekhov.railway.exception.RailException;
import by.aterekhov.railway.train.Train;
import by.aterekhov.railway.trainpart.RailElement;
import by.aterekhov.railway.trainpart.carriage.sort.RailFreightCarriage;
import by.aterekhov.railway.trainpart.carriage.sort.RailPassengerCarriage;
import by.aterekhov.railway.trainpart.locomotive.RailLocomotive;
import by.aterekhov.railway.validation.Validator;

public class TrainCreator {

    public static Train createTrain(String trName) throws RailException
    {
        if(!Validator.checkTrainName(trName))
        {
            throw new RailException(ProjectConstant.MSG_WRONG_TRAIN_NAME);
        }
        return new Train(trName);
    }

    public static void addLocomotive(Train train,
                                      String railName,
                                      double gaugeValue,
                                      double lengthValue,
                                      double widthValue,
                                      double heightValue,
                                      double weightValue,
                                      String locomotiveType,
                                      double powerValue,
                                      double maxSpeed) throws RailException
    {
        if(Validator.checkLocomotive(railName,
        gaugeValue,
        lengthValue,
        widthValue,
        heightValue,
        weightValue,
        locomotiveType,
        powerValue,
        maxSpeed))
        {
            RailElement element = new RailLocomotive(railName,
                                                    gaugeValue,
                                                    lengthValue,
                                                    widthValue,
                                                    heightValue,
                                                    weightValue,
                                                    locomotiveType,
                                                    powerValue,
                                                    maxSpeed);
            train.addTrainElement(element);
        }
        else{
            throw new RailException(ProjectConstant.MSG_WRONG_LOCOMOTIVE);
        }

    }

    public static void addFreightCarriage(Train train,
                                      String railName,
                                      double gaugeValue,
                                      double lengthValue,
                                      double widthValue,
                                      double heightValue,
                                      double weightValue,
                                      String freightCarriageKind,
                                      double carryingCapacity) throws RailException
    {
        if(Validator.checkFreightCarriage(railName,
                gaugeValue,
                lengthValue,
                widthValue,
                heightValue,
                weightValue,
                freightCarriageKind,
                carryingCapacity))
        {
            RailElement element = new RailFreightCarriage(railName,
                    gaugeValue,
                    lengthValue,
                    widthValue,
                    heightValue,
                    weightValue,
                    freightCarriageKind,
                    carryingCapacity);
            train.addTrainElement(element);
        }
        else{
            throw new RailException(ProjectConstant.MSG_WRONG_FREIGHT);
        }

    }

    public static void addPassengerCarriage(Train train,
                                           String railName,
                                           double gaugeValue,
                                           double lengthValue,
                                           double widthValue,
                                           double heightValue,
                                           double weightValue,
                                           String passengerCarriageRank,
                                           int passengerAmount,
                                           int passengerBaggageAmount) throws RailException
    {
        if(Validator.checkPassengerCarriage(railName,
                gaugeValue,
                lengthValue,
                widthValue,
                heightValue,
                weightValue,
                passengerCarriageRank,
                passengerAmount,
                passengerBaggageAmount))
        {
            RailElement element = new RailPassengerCarriage(railName,
                    gaugeValue,
                    lengthValue,
                    widthValue,
                    heightValue,
                    weightValue,
                    passengerCarriageRank,
                    passengerAmount,
                    passengerBaggageAmount);
            train.addTrainElement(element);
        }
        else{
            throw new RailException(ProjectConstant.MSG_WRONG_PASSENGER);
        }

    }

}
