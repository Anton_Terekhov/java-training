package by.aterekhov.railway.constant;

public class ProjectConstant {
    //Train creation
    public static final String MSG_WRONG_TRAIN_NAME = "Train's name isn't valid. Name should be not empty and less than 100 symbols.";
    public static final String MSG_WRONG_LOCOMOTIVE = "New train's locomotive isn't valid. Please, try again.";
    public static final String MSG_WRONG_FREIGHT = "New train's freight carriage isn't valid. Please, try again.";
    public static final String MSG_WRONG_PASSENGER = "New train's passenger carriage isn't valid. Please, try again.";

    //Validator
    public static final double MIN_COMMON_DOUBLE = 0;
    public static final int MIN_COMMON_INT = 0;
    public static final int MAX_RAIL_NAME_LEN = 100;
    public static final double MAX_GAUGE_VALUE = 5000;
    public static final double MAX_LENGTH_VALUE = 20000;
    public static final double MAX_WIDTH_VALUE = 5000;
    public static final double MAX_HEIGHT_VALUE = 7000;
    public static final double MAX_WEIGHT_VALUE = 100000;
    public static final double MAX_POWER_VALUE = 10000;
    public static final double MAX_SPEED_VALUE = 400;
    public static final double MAX_CAPACITY_VALUE = 250000;
    public static final int MAX_PASSENGER_VALUE = 400;
    public static final int MAX_BAGGAGE_VALUE = 250000;

    //File info
    public static final String FILE_PATH = "data/";
    public static final String FILE_NAME = "Train_654B.xlsx";

    //Search params
    public static final int MIN_PASSENGER_SEARCH = 15;
    public static final int MAX_PASSENGER_SEARCH = 40;

    private ProjectConstant() {};
}
