package by.aterekhov.railway.trainpart.carriage.sort;

import by.aterekhov.railway.trainpart.carriage.RailCarriage;

public class RailFreightCarriage extends RailCarriage {
    private FreightCarriageKind freightCarriageKind;
    private double carryingCapacity;

    public RailFreightCarriage(String railName,
                               double gaugeValue,
                               double lengthValue,
                               double widthValue,
                               double heightValue,
                               double weightValue,
                               String freightCarriageKind,
                               double carryingCapacity) {
        super(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue, "FREIGHT");
        this.freightCarriageKind = FreightCarriageKind.valueOf(freightCarriageKind.toUpperCase()) ;
        this.carryingCapacity = carryingCapacity;
    }

    public String getFreightCarriageKind() {
        return freightCarriageKind.name();
    }

    public void setFreightCarriageKind( String freightCarriageKind) {
        this.freightCarriageKind = FreightCarriageKind.valueOf(freightCarriageKind);
    }

    public double getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(double carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public String toString()
    {
        return ("Freight carriage: '" + super.getRailName() + "' (" + super.getGaugeValue() + " mm gauge base)" +
                ", carriage type is " + freightCarriageKind.name().toLowerCase() +
                ", capacity = " + carryingCapacity + " kg" +
                ", size is " + super.getLengthValue() + " x " + super.getHeightValue() + " x " + super.getWeightValue() +
                ", " + super.getWidthValue() + " kg."
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RailFreightCarriage freightCarriage = (RailFreightCarriage) o;

        if (Double.compare(freightCarriage.carryingCapacity, carryingCapacity) != 0) return false;
        return freightCarriageKind.name().equals(freightCarriage.freightCarriageKind.name());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + freightCarriageKind.name().hashCode();
        temp = Double.doubleToLongBits(carryingCapacity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

enum FreightCarriageKind {TANKER, OPEN_CARRIAGE, TRUCK};