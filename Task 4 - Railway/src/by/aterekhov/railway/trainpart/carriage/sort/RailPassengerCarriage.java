package by.aterekhov.railway.trainpart.carriage.sort;

import by.aterekhov.railway.trainpart.carriage.RailCarriage;

public class RailPassengerCarriage extends RailCarriage {

    private PassengerCarriageRank passengerCarriageRank;
    private int passengerAmount;
    private int passengerBaggageAmount;

    public RailPassengerCarriage(String railName,
                                 double gaugeValue,
                                 double lengthValue,
                                 double widthValue,
                                 double heightValue,
                                 double weightValue,
                                 String passengerCarriageRank,
                                 int passengerAmount,
                                 int passengerBaggageAmount)
    {
        super(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue, "PASSENGER");
        this.passengerCarriageRank = PassengerCarriageRank.valueOf(passengerCarriageRank.toUpperCase());
        this.passengerAmount = passengerAmount;
        this.passengerBaggageAmount = passengerBaggageAmount;
    }

    public String getPassengerCarriageRank() {
        return passengerCarriageRank.name();
    }

    public void setPassengerCarriageRank(String passengerCarriageRank) {
        this.passengerCarriageRank = PassengerCarriageRank.valueOf(passengerCarriageRank.toUpperCase());
    }

    public int getPassengerAmount() {
        return passengerAmount;
    }

    public void setPassengerAmount(int passengerAmount) {
        this.passengerAmount = passengerAmount;
    }

    public int getPassengerBaggageAmount() {
        return passengerBaggageAmount;
    }

    public void setPassengerBaggageAmount(int passengerBaggageAmount) {
        this.passengerBaggageAmount = passengerBaggageAmount;
    }

    @Override
    public String toString()
    {
        return ("Passenger carriage: '" + super.getRailName() + "' (" + super.getGaugeValue() + " mm gauge base)" +
                ", carriage rank is " + passengerCarriageRank.name().toLowerCase() +
                ", max passengers = " + passengerAmount + " persons" +
                ", max amount of baggage = " + passengerBaggageAmount + " items" +
                ", size is " + super.getLengthValue() + " x " + super.getHeightValue() + " x " + super.getWeightValue() +
                ", " + super.getWidthValue() + " kg."
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RailPassengerCarriage passengerCarriage = (RailPassengerCarriage) o;

        if (passengerAmount != passengerCarriage.passengerAmount) return false;
        if (passengerBaggageAmount != passengerCarriage.passengerBaggageAmount) return false;
        return passengerCarriageRank.name().equals(passengerCarriage.passengerCarriageRank.name());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + passengerCarriageRank.name().hashCode();
        result = 31 * result + passengerAmount;
        result = 31 * result + passengerBaggageAmount;
        return result;
    }
}

enum PassengerCarriageRank {BUSINESS, ECONOMIC};