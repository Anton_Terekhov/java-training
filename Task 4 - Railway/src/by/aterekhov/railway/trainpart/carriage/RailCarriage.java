package by.aterekhov.railway.trainpart.carriage;

import by.aterekhov.railway.trainpart.RailElement;

public class RailCarriage extends RailElement {
    private CarriageType carriageType;

    public RailCarriage(String railName,
                        double gaugeValue,
                        double lengthValue,
                        double widthValue,
                        double heightValue,
                        double weightValue,
                        String carriageType)
    {
        super(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue);
        this.carriageType = CarriageType.valueOf(carriageType.toUpperCase());
    }

    public String getCarriageType() {
        return carriageType.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RailCarriage carriage = (RailCarriage) o;

        return carriageType.name().equals(carriage.carriageType.name());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + carriageType.name().hashCode();
        return result;
    }
}

enum CarriageType {PASSENGER, FREIGHT};