package by.aterekhov.railway.trainpart.locomotive;

import by.aterekhov.railway.trainpart.RailElement;

public class RailLocomotive extends RailElement {

    private LocomotiveType locomotiveType;
    private double powerValue;
    private double maxSpeed;

    public RailLocomotive(String railName,
                          double gaugeValue,
                          double lengthValue,
                          double widthValue,
                          double heightValue,
                          double weightValue,
                          String locomotiveType,
                          double powerValue,
                          double maxSpeed)
    {
        super(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue);
        this.locomotiveType = LocomotiveType.valueOf(locomotiveType.toUpperCase());
        this.powerValue = powerValue;
        this.maxSpeed = maxSpeed;
    }

    public String getLocomotiveType() {
        return locomotiveType.name();
    }

    public void setLocomotiveType(String locomotiveType) {
        this.locomotiveType = LocomotiveType.valueOf(locomotiveType.toUpperCase()) ;
    }

    public double getPowerValue() {
        return powerValue;
    }

    public void setPowerValue(double powerValue) {
        this.powerValue = powerValue;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString()
    {
        return ("Locomotive: '" + super.getRailName() + "' (" + super.getGaugeValue() + " mm gauge base)" +
                ", engine type is " + locomotiveType.name().toLowerCase() +
                ", power = " + powerValue + " hp" +
                ", max speed = " + maxSpeed + " km/h " +
                ", size is " + super.getLengthValue() + " x " + super.getHeightValue() + " x " + super.getWeightValue() +
                ", " + super.getWidthValue() + " kg."
                );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RailLocomotive locomotive = (RailLocomotive) o;

        if (Double.compare(locomotive.powerValue, powerValue) != 0) return false;
        if (Double.compare(locomotive.maxSpeed, maxSpeed) != 0) return false;
        return locomotiveType.name().equals(locomotive.locomotiveType.name());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + locomotiveType.name().hashCode();
        temp = Double.doubleToLongBits(powerValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxSpeed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

enum LocomotiveType {DIESEL, ELECTRIC};