package by.aterekhov.railway.trainpart;

public abstract class RailElement {
    private String railName;

    private double gaugeValue;

    private double lengthValue;
    private double widthValue;
    private double heightValue;

    private double weightValue;

    //Constructor
    public RailElement(String railName, double gaugeValue, double lengthValue, double widthValue, double heightValue, double weightValue)
    {
        this.railName = railName;
        this.gaugeValue = gaugeValue;
        this.lengthValue = lengthValue;
        this.widthValue = widthValue;
        this.heightValue = heightValue;
        this.weightValue = weightValue;
    }

    //railName
    public String getRailName() {
        return railName;
    }

    public void setRailName(String railName) {
        this.railName = railName;
    }

    //gaugeValue
    public double getGaugeValue() {
        return gaugeValue;
    }

    public void setGaugeValue(double gaugeValue) {
        this.gaugeValue = gaugeValue;
    }

    //lengthValue
    public double getLengthValue() {
        return lengthValue;
    }

    public void setLengthValue(double lengthValue) {
        this.lengthValue = lengthValue;
    }

    //getWidthValue
    public double getWidthValue() {
        return widthValue;
    }

    public void setWidthValue(double widthValue) {
        this.widthValue = widthValue;
    }

    //getHeightValue
    public double getHeightValue() {
        return heightValue;
    }

    public void setHeightValue(double heightValue) {
        this.heightValue = heightValue;
    }

    //getWeightValue
    public double getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(double weightValue) {
        this.weightValue = weightValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RailElement element = (RailElement) o;

        if (Double.compare(element.gaugeValue, gaugeValue) != 0) return false;
        if (Double.compare(element.lengthValue, lengthValue) != 0) return false;
        if (Double.compare(element.widthValue, widthValue) != 0) return false;
        if (Double.compare(element.heightValue, heightValue) != 0) return false;
        if (Double.compare(element.weightValue, weightValue) != 0) return false;
        return railName.equals(element.railName);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = railName.hashCode();
        temp = Double.doubleToLongBits(gaugeValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lengthValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(widthValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(heightValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(weightValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
