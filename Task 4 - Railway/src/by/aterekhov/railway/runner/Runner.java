package by.aterekhov.railway.runner;

import by.aterekhov.railway.action.TrainAction;
import by.aterekhov.railway.constant.ProjectConstant;
import by.aterekhov.railway.exception.RailException;
import by.aterekhov.railway.iooperation.InputTrain;
import by.aterekhov.railway.train.Train;

public class Runner {

    public static void main(String args[]){

        Train train;
        train = InputTrain.inputTrainData(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        TrainAction.calculateTrainPassengers(train);
        TrainAction.calculateTrainBaggage(train);

        TrainAction.sortTrain(train);
        TrainAction.searchPassengerCarriage(train, ProjectConstant.MIN_PASSENGER_SEARCH,ProjectConstant.MAX_PASSENGER_SEARCH);
    }
}
