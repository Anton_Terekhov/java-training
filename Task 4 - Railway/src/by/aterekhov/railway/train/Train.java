package by.aterekhov.railway.train;

import by.aterekhov.railway.trainpart.RailElement;
import by.aterekhov.railway.trainpart.carriage.sort.RailPassengerCarriage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Train {
    private ArrayList<RailElement> train = new ArrayList<RailElement>();
    private String trainName;
    private final static Logger logger = LogManager.getLogger(Train.class);

    public Train(String trainName)
    {
        this.trainName = trainName;
        logger.info("Train object was created. Train name: '" + trainName +"'");
    }

    public void addTrainElement(RailElement el)
    {
        train.add(el);
        logger.info("New element '" + el.getRailName() + "' was added to the train.");
        logger.info(el.toString());
    }

    public ArrayList<RailElement> returnAllTrain()
    {
        return train;
    }

    @Override
    public String toString()
    {
        String result = "";
        for(RailElement el : train)
        {
            result+=(el.toString() + "\n");
        }
        return result;
    }

    public String getTrainName()
    {
        return trainName;
    }

    public void setTrainName(String trainName)
    {
        this.trainName = trainName;
    }

}
