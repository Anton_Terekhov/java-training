package by.aterekhov.railway.train;


import by.aterekhov.railway.trainpart.RailElement;
import by.aterekhov.railway.trainpart.carriage.RailCarriage;
import by.aterekhov.railway.trainpart.carriage.sort.RailFreightCarriage;
import by.aterekhov.railway.trainpart.carriage.sort.RailPassengerCarriage;
import by.aterekhov.railway.trainpart.locomotive.RailLocomotive;

import java.util.Comparator;

public class TrainComparatorByRailElement implements Comparator<RailElement>{

    @Override
    public int compare(RailElement el1, RailElement el2)
    {
        if(el1 instanceof RailLocomotive && el2 instanceof RailLocomotive)
        {
            return 0;
        }
        if(el1 instanceof RailLocomotive && el2 instanceof RailCarriage)
        {
            return -1;
        }
        if(el1 instanceof RailCarriage && el2 instanceof RailLocomotive)
        {
            return 1;
        }
        if(el1 instanceof RailPassengerCarriage && el2 instanceof RailFreightCarriage)
        {
            return -1;
        }
        if(el1 instanceof RailPassengerCarriage && el2 instanceof RailPassengerCarriage)
        {
            return 0;
        }
        if(el1 instanceof RailFreightCarriage && el2 instanceof RailPassengerCarriage)
        {
            return 1;
        }
        return 0;
    }
}
