package by.aterekhov.railway.train;

import by.aterekhov.railway.trainpart.RailElement;
import by.aterekhov.railway.trainpart.carriage.RailCarriage;
import by.aterekhov.railway.trainpart.carriage.sort.RailFreightCarriage;
import by.aterekhov.railway.trainpart.carriage.sort.RailPassengerCarriage;
import by.aterekhov.railway.trainpart.locomotive.RailLocomotive;

import java.util.Comparator;

public class TrainComparatorByMainElementValue implements Comparator<RailElement> {

    @Override
    public int compare(RailElement el1, RailElement el2)
    {
        if(el1 instanceof RailLocomotive && el2 instanceof RailLocomotive)
        {
            return Double.compare( ((RailLocomotive) el2).getPowerValue(), ((RailLocomotive) el1).getPowerValue() );
        }
        if(el1 instanceof RailPassengerCarriage && el2 instanceof RailPassengerCarriage)
        {
            return Double.compare( ((RailPassengerCarriage) el2).getPassengerAmount(), ((RailPassengerCarriage) el1).getPassengerAmount() );
        }
        if(el1 instanceof RailFreightCarriage && el2 instanceof RailFreightCarriage)
        {
            return Double.compare( ((RailFreightCarriage) el2).getCarryingCapacity(), ((RailFreightCarriage) el1).getCarryingCapacity() );
        }
        return 0;
    }
}
