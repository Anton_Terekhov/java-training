package by.aterekhov.railway.validation;

import by.aterekhov.railway.constant.ProjectConstant;

public class Validator {

    public static boolean checkTrainName(String trName)
    {
        return (!trName.isEmpty() && trName.length()<ProjectConstant.MAX_RAIL_NAME_LEN );
    }

    private static boolean checkRailElement(String railName,
                                            double gaugeValue,
                                            double lengthValue,
                                            double widthValue,
                                            double heightValue,
                                            double weightValue)
    {
        return (!railName.isEmpty() && railName.length()< ProjectConstant.MAX_RAIL_NAME_LEN &&
                gaugeValue>ProjectConstant.MIN_COMMON_DOUBLE && gaugeValue<ProjectConstant.MAX_GAUGE_VALUE &&
                lengthValue>ProjectConstant.MIN_COMMON_DOUBLE && lengthValue<ProjectConstant.MAX_LENGTH_VALUE &&
                widthValue>ProjectConstant.MIN_COMMON_DOUBLE && widthValue<ProjectConstant.MAX_WIDTH_VALUE &&
                heightValue>ProjectConstant.MIN_COMMON_DOUBLE && heightValue<ProjectConstant.MAX_HEIGHT_VALUE &&
                weightValue>ProjectConstant.MIN_COMMON_DOUBLE && weightValue<ProjectConstant.MAX_WEIGHT_VALUE
        );
    }

    public static boolean checkLocomotive(String railName,
                                          double gaugeValue,
                                          double lengthValue,
                                          double widthValue,
                                          double heightValue,
                                          double weightValue,
                                          String locomotiveType,
                                          double powerValue,
                                          double maxSpeed)
    {
        return (checkRailElement(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue) &&
                (locomotiveType.toUpperCase().equals("DIESEL") || locomotiveType.toUpperCase().equals("ELECTRIC") ) &&
                powerValue>ProjectConstant.MIN_COMMON_DOUBLE && powerValue<ProjectConstant.MAX_POWER_VALUE &&
                maxSpeed>ProjectConstant.MIN_COMMON_DOUBLE && maxSpeed<ProjectConstant.MAX_SPEED_VALUE
        );
    }

    public static boolean checkFreightCarriage(String railName,
                                          double gaugeValue,
                                          double lengthValue,
                                          double widthValue,
                                          double heightValue,
                                          double weightValue,
                                          String freightCarriageKind,
                                          double carryingCapacity)
    {
        return (checkRailElement(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue) &&
                (freightCarriageKind.toUpperCase().equals("TANKER") || freightCarriageKind.toUpperCase().equals("OPEN_CARRIAGE") || freightCarriageKind.toUpperCase().equals("TRUCK") ) &&
                carryingCapacity>ProjectConstant.MIN_COMMON_DOUBLE && carryingCapacity<ProjectConstant.MAX_CAPACITY_VALUE
        );
    }

    public static boolean checkPassengerCarriage(String railName,
                                          double gaugeValue,
                                          double lengthValue,
                                          double widthValue,
                                          double heightValue,
                                          double weightValue,
                                          String passengerCarriageRank,
                                          int passengerAmount,
                                          int passengerBaggageAmount)
    {
        return (checkRailElement(railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue) &&
                (passengerCarriageRank.toUpperCase().equals("BUSINESS") || passengerCarriageRank.toUpperCase().equals("ECONOMIC") ) &&
                passengerAmount>ProjectConstant.MIN_COMMON_INT && passengerAmount<ProjectConstant.MAX_PASSENGER_VALUE &&
                passengerBaggageAmount>ProjectConstant.MIN_COMMON_INT && passengerBaggageAmount <ProjectConstant.MAX_BAGGAGE_VALUE
        );
    }

}
