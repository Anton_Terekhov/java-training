package by.aterekhov.railway.iooperation;

import by.aterekhov.railway.creator.TrainCreator;
import by.aterekhov.railway.exception.RailException;
import by.aterekhov.railway.train.Train;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Iterator;

public class InputTrain {
    private final static Logger logger = LogManager.getLogger(InputTrain.class);

    public static Train inputTrainData(String filePath, String fileName)
    {
        Train train = null;

        try {

            File file = new File(filePath + fileName);
            FileInputStream fis = new FileInputStream(file);

            String[] trainName = fileName.split("\\.",2);

            train = TrainCreator.createTrain(trainName[0]);

            // Finds the workbook instance for XLSX file
            XSSFWorkbook workbook = new XSSFWorkbook (fis);

            // Return first sheet from the XLSX workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            // Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = sheet.iterator();

            //Skip first row
            rowIterator.next();

            // Traversing over each row of XLSX file
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();

                String railName = row.getCell(0).toString();
                String railType = row.getCell(1).toString();
                double gaugeValue = row.getCell(2).getNumericCellValue();
                double lengthValue = row.getCell(3).getNumericCellValue();
                double widthValue = row.getCell(4).getNumericCellValue();
                double heightValue = row.getCell(5).getNumericCellValue();
                double weightValue = row.getCell(6).getNumericCellValue();
                String locomotiveType = row.getCell(7).toString();

                switch (railType)
                {
                    case "Locomotive":
                        double powerValue = row.getCell(8).getNumericCellValue();
                        double maxSpeed = row.getCell(9).getNumericCellValue();
                        TrainCreator.addLocomotive(train, railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue, locomotiveType, powerValue, maxSpeed);
                        break;
                    case "FreightCarriage":
                        String freightCarriageKind = row.getCell(10).toString();
                        double carryingCapacity = row.getCell(11).getNumericCellValue();
                        TrainCreator.addFreightCarriage(train, railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue, freightCarriageKind, carryingCapacity);
                        break;
                    case "PassengerCarriage":
                        String passengerCarriageRank = row.getCell(12).toString();
                        int passengerAmount = (int) row.getCell(13).getNumericCellValue();
                        int passengerBaggageAmount = (int) row.getCell(14).getNumericCellValue();
                        TrainCreator.addPassengerCarriage(train, railName, gaugeValue, lengthValue, widthValue, heightValue, weightValue, passengerCarriageRank, passengerAmount, passengerBaggageAmount);
                        break;
                    default:
                        logger.warn("In file there are unspecified rail element type. Type is '" + railType + "'.");
                        break;
                }

                /*
                //Debug info
                // For each row, iterate through each columns
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext())
                {
                    Cell cell = cellIterator.next();
                    System.out.print(cell.toString());
                } System.out.println("");
                */
            }

        }
        catch (RailException e)
        {
            logger.warn("Train creation catches issue.");
            logger.warn("Details:\n" + e.toString() );
        }
        catch(ClassCastException e)
        {
            logger.warn("Wrong casting.");
            logger.warn("Details:\n" + e.toString() );
        }
        catch(IOException e)
        {
            logger.warn("Wrong file read/write operation.");
            logger.warn("Details:\n" + e.toString() );
        }

        return train;
    }
}
