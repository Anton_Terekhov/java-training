package test.by.aterekhov.railway;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.by.aterekhov.railway.action.TrainActionTest;
import test.by.aterekhov.railway.creator.TrainCreatorTest;

@Suite.SuiteClasses({TrainActionTest.class, TrainCreatorTest.class})
@RunWith(Suite.class)
public class TrainSuite {
}
