package test.by.aterekhov.railway.action;

import by.aterekhov.railway.action.TrainAction;
import by.aterekhov.railway.constant.ProjectConstant;
import by.aterekhov.railway.iooperation.InputTrain;
import by.aterekhov.railway.train.Train;
import org.junit.Assert;
import org.junit.Test;

public class TrainActionTest {

    @Test
    public void calculateTrainPassengersTest()
    {
        Train train;
        train = InputTrain.inputTrainData(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        int actual = TrainAction.calculateTrainPassengers(train);
        int expected = 81;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void calculateTrainBaggageTest()
    {
        Train train;
        train = InputTrain.inputTrainData(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        int actual = TrainAction.calculateTrainBaggage(train);
        int expected = 187;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void searchPassengerCarriageTest()
    {
        Train train;
        train = InputTrain.inputTrainData(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        int actual = TrainAction.searchPassengerCarriage(train, ProjectConstant.MIN_PASSENGER_SEARCH,ProjectConstant.MAX_PASSENGER_SEARCH).size();
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }
}
