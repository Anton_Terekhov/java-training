package test.by.aterekhov.railway.creator;

import by.aterekhov.railway.creator.TrainCreator;
import by.aterekhov.railway.exception.RailException;
import by.aterekhov.railway.train.Train;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TrainCreatorTest {

    private String trName;
    private String outTrName;


    public TrainCreatorTest(String trName, String outTrName)
    {
        this.trName = trName;
        this.outTrName = outTrName;
    }

    @Parameters
    public static Collection<Object[]> nameExamples() {
        return Arrays.asList(new Object[][]{
                {" ", " "},
                {"Some train!","Some train!"},
                {"123123", "123123"},
                {"   ", "   "}
        });
    }

    @Test
    public void checkTrainCreation() throws RailException
    {
        Train train;
        train = TrainCreator.createTrain(this.trName);
        String expected = this.outTrName;
        String actual = train.getTrainName();
        Assert.assertEquals(expected, actual);

    }
}
