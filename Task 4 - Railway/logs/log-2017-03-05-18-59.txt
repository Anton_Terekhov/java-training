2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Train object was created. Train name: 'Train_654B'
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive Strela K705' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive Strela K705' (1250.0 mm gauge base), engine type is diesel, power = 2500.0 hp, max speed = 115.0 km/h , size is 12000.0 x 4000.0 x 60000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Oil carriage F14' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Freight carriage: 'Oil carriage F14' (1250.0 mm gauge base), carriage type is tanker, capacity = 150000.0 kg, size is 14000.0 x 3500.0 x 21500.0, 2800.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Common carriage CK78' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Business carriage H78' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Car carriage FFFT' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Freight carriage: 'Car carriage FFFT' (1250.0 mm gauge base), carriage type is truck, capacity = 65000.0 kg, size is 12000.0 x 3000.0 x 27500.0, 2000.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive LS508' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive LS508' (1250.0 mm gauge base), engine type is diesel, power = 1050.0 hp, max speed = 50.0 km/h , size is 7000.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive LL1' was added to the train.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive LL1' (1250.0 mm gauge base), engine type is diesel, power = 1500.0 hp, max speed = 65.0 km/h , size is 150.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] WARN  by.aterekhov.railway.iooperation.InputTrain - In file there are unspecified rail element type. Type is 'Car'.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passengers for train 'Train_654B' was finished, amount of passengers = 81.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passenger's baggage for train 'Train_654B' was finished, amount of baggage = 187 items.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - Train was sorted. Details are presented below.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #1: Locomotive: 'Locomotive Strela K705' (1250.0 mm gauge base), engine type is diesel, power = 2500.0 hp, max speed = 115.0 km/h , size is 12000.0 x 4000.0 x 60000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #2: Locomotive: 'Locomotive LL1' (1250.0 mm gauge base), engine type is diesel, power = 1500.0 hp, max speed = 65.0 km/h , size is 150.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #3: Locomotive: 'Locomotive LS508' (1250.0 mm gauge base), engine type is diesel, power = 1050.0 hp, max speed = 50.0 km/h , size is 7000.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #4: Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #5: Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #6: Freight carriage: 'Oil carriage F14' (1250.0 mm gauge base), carriage type is tanker, capacity = 150000.0 kg, size is 14000.0 x 3500.0 x 21500.0, 2800.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #7: Freight carriage: 'Car carriage FFFT' (1250.0 mm gauge base), carriage type is truck, capacity = 65000.0 kg, size is 12000.0 x 3000.0 x 27500.0, 2000.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - Search of passenger carriages with passenger amount between 15 and 80 was finished. Total carriages: 2.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - Detailed results are:
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #1: Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:11 [main] INFO  by.aterekhov.railway.action.TrainAction - #2: Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Train object was created. Train name: 'Train_654B'
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive Strela K705' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive Strela K705' (1250.0 mm gauge base), engine type is diesel, power = 2500.0 hp, max speed = 115.0 km/h , size is 12000.0 x 4000.0 x 60000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Oil carriage F14' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Freight carriage: 'Oil carriage F14' (1250.0 mm gauge base), carriage type is tanker, capacity = 150000.0 kg, size is 14000.0 x 3500.0 x 21500.0, 2800.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Common carriage CK78' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Business carriage H78' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Car carriage FFFT' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Freight carriage: 'Car carriage FFFT' (1250.0 mm gauge base), carriage type is truck, capacity = 65000.0 kg, size is 12000.0 x 3000.0 x 27500.0, 2000.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive LS508' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive LS508' (1250.0 mm gauge base), engine type is diesel, power = 1050.0 hp, max speed = 50.0 km/h , size is 7000.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive LL1' was added to the train.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive LL1' (1250.0 mm gauge base), engine type is diesel, power = 1500.0 hp, max speed = 65.0 km/h , size is 150.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] WARN  by.aterekhov.railway.iooperation.InputTrain - In file there are unspecified rail element type. Type is 'Car'.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passengers for train 'Train_654B' was finished, amount of passengers = 81.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passenger's baggage for train 'Train_654B' was finished, amount of baggage = 187 items.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - Train was sorted. Details are presented below.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #1: Locomotive: 'Locomotive Strela K705' (1250.0 mm gauge base), engine type is diesel, power = 2500.0 hp, max speed = 115.0 km/h , size is 12000.0 x 4000.0 x 60000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #2: Locomotive: 'Locomotive LL1' (1250.0 mm gauge base), engine type is diesel, power = 1500.0 hp, max speed = 65.0 km/h , size is 150.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #3: Locomotive: 'Locomotive LS508' (1250.0 mm gauge base), engine type is diesel, power = 1050.0 hp, max speed = 50.0 km/h , size is 7000.0 x 3500.0 x 50000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #4: Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #5: Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #6: Freight carriage: 'Oil carriage F14' (1250.0 mm gauge base), carriage type is tanker, capacity = 150000.0 kg, size is 14000.0 x 3500.0 x 21500.0, 2800.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #7: Freight carriage: 'Car carriage FFFT' (1250.0 mm gauge base), carriage type is truck, capacity = 65000.0 kg, size is 12000.0 x 3000.0 x 27500.0, 2000.0 kg.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - Search of passenger carriages with passenger amount between 15 and 40 was finished. Total carriages: 1.
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - Detailed results are:
2017-03-05 18:59:32 [main] INFO  by.aterekhov.railway.action.TrainAction - #1: Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
