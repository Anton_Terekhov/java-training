2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - Train object was created. Train name: 'Train_654B'
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - New element 'Locomotive Strela K705' was added to the train.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - Locomotive: 'Locomotive Strela K705' (1250.0 mm gauge base), engine type is diesel, power = 2500.0 hp, max speed = 115.0 km/h , size is 12000.0 x 4000.0 x 60000.0, 2500.0 kg.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - New element 'Oil carriage F14' was added to the train.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - Freight carriage: 'Oil carriage F14' (1250.0 mm gauge base), carriage type is tanker, capacity = 150000.0 kg, size is 14000.0 x 3500.0 x 21500.0, 2800.0 kg.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - New element 'Common carriage CK78' was added to the train.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Common carriage CK78' (1250.0 mm gauge base), carriage rank is business, max passengers = 56 persons, max amount of baggage = 112 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - New element 'Business carriage H78' was added to the train.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.train.Train - Passenger carriage: 'Business carriage H78' (1250.0 mm gauge base), carriage rank is business, max passengers = 25 persons, max amount of baggage = 75 items, size is 15000.0 x 3800.0 x 32000.0, 2500.0 kg.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passengers for train 'Train_654B' was finished, amount of passengers = 81.
2017-02-27 17:17:57 [main] INFO  by.aterekhov.railway.action.TrainAction - Calculation of passenger's baggage for train 'Train_654B' was finished, amount of baggage = 187 items.
