package by.aterekhov.geometry.inputoutputoperation;

public class ProjectConstant {
    //Communication
    public static final String MSG_WELCOME = "Hello, it's application that provides geometry calculation with common shapes.";
    public static final String MSG_CHOICE_SHAPE = "Please, type 1 to work with Circle shapes, type 2 to work with another shapes (not implemented now).";
    public static final String MSG_CHOICE_INPUT = "Please, type 1 to input data through CMD, type 2 to input through File.";
    public static final String MSG_CHOICE_OUTPUT = "Please, type 1 to output data through CMD, type 2 to output through File, type 3 to output through both stream.";
    public static final String MSG_CHOICE_NEXT_STEP = "Please, type 0  to exit, type 1 to start from beginning.";
    public static final String MSG_WRONG_VALUE = "Wrong value, try again.";
    public static final String MSG_NOT_IMPLEMENTED = "This functionality not implemented, it's just example of app work.";

    //IO
    public static final String MSG_INPUT_COUNT_RADIAL = "Please specify how many radials you want to enter: ";
    public static final String MSG_INPUT_RADIAL_LIST = "Radials are: ";
    public static final String MSG_OUTPUT_FILE_NOTIFICATION = "File was written.";

    //FilePath
    public static final String PATH_INPUT = "data/InputDataCircle.txt";
    public static final String PATH_OUTPUT = "data/OutputDataCircle.txt";

    private ProjectConstant() {};

}
