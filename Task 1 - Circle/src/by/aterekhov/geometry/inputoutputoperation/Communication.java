package by.aterekhov.geometry.inputoutputoperation;

import by.aterekhov.geometry.inputoutputoperation.iocmd.InputCmdCircle;
import by.aterekhov.geometry.inputoutputoperation.iocmd.OutputCmdCircle;
import by.aterekhov.geometry.inputoutputoperation.iofile.InputFileCircle;
import by.aterekhov.geometry.inputoutputoperation.iofile.OutputFileCircle;

import by.aterekhov.geometry.action.circle.CircleAction;
import by.aterekhov.geometry.creator.CircleCreator;
import by.aterekhov.geometry.shape.circle.Circle;

import java.util.ArrayList;
import java.util.Scanner;

public class Communication {

    private ArrayList<Circle> circles = new ArrayList<Circle>();

    private void addCircle(ArrayList<String> strArray)
    {
        double dblRadial;
        for(String strCurrent : strArray)
        {
            Circle circle;

            try
            {
                dblRadial = Double.parseDouble(strCurrent);
                circle = CircleCreator.createCircle(dblRadial);
                circles.add(circle);
            }
            catch (Exception e)
            {
                System.out.println("Warning: there are wrong radials.");
            }
        }
    }

    private ArrayList<String> resultCircleCalculation()
    {
        ArrayList<String> strResults = new ArrayList<String>();
        String strCurrentResult = "";
        for(Circle c : circles)
        {
            strCurrentResult="Shape: '" + c.getName() +
                    "', Radial: " + c.getRadial() +
                    ", Perimeter: " + CircleAction.calculatePerimeter(c) +
                    ", Area: " + CircleAction.calculateArea(c) + ".\n";

            strResults.add(strCurrentResult);
        }
        return strResults;
    }


    private void choiceShape()
    {
        int menuOption;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println(ProjectConstant.MSG_CHOICE_SHAPE);

            //input of some digit
            if (scanner.hasNextInt()) {
                menuOption = scanner.nextInt();


                switch (menuOption)
                {
                    //Circle
                    case 1:
                        choiceInput(scanner);
                        return;
                    //Other shapes, not implemented
                    case 2:
                        System.out.println(ProjectConstant.MSG_NOT_IMPLEMENTED);
                        return;
                    default:
                        System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                }
            }
            //not digit
            else
            {
                System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                scanner.next();
            }
        } while(true);

    }

    private void choiceInput(Scanner scanner)
    {
        int menuOption;
        ArrayList<String> inRadials;

        do {
            System.out.println(ProjectConstant.MSG_CHOICE_INPUT);

            //input of some digit
            if (scanner.hasNextInt()) {
                menuOption = scanner.nextInt();

                switch (menuOption)
                {
                    //CMD
                    case 1:
                        inRadials = InputCmdCircle.inputCmdCircle(scanner);
                        addCircle(inRadials);
                        choiceOutput(scanner);
                        return;
                    //File
                    case 2:
                        inRadials = InputFileCircle.inputFileCircle();
                        addCircle(inRadials);
                        choiceOutput(scanner);
                        return;
                    default:
                        System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                }
            }
            //not digit
            else
            {
                System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                scanner.next();
            }
        } while(true);
    }

    private void choiceOutput(Scanner scanner)
    {
        int menuOption;

        do {
            System.out.println(ProjectConstant.MSG_CHOICE_OUTPUT);

            //input of some digit
            if (scanner.hasNextInt()) {
                menuOption = scanner.nextInt();

                switch (menuOption)
                {
                    //CMD
                    case 1:
                        OutputCmdCircle.outputCmdCircle(resultCircleCalculation());
                        return;
                    //File
                    case 2:
                        OutputFileCircle.outputFileCircle(resultCircleCalculation());
                        return;
                    //CMD + File
                    case 3:
                        OutputCmdCircle.outputCmdCircle(resultCircleCalculation());
                        OutputFileCircle.outputFileCircle(resultCircleCalculation());
                        return;
                    default:
                        System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                }
            }
            //not digit
            else
            {
                System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                scanner.next();
            }
        } while(true);
    }

    private int choiceNextStep()
    {
        int menuOption;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println(ProjectConstant.MSG_CHOICE_NEXT_STEP);

            //input of some digit
            if (scanner.hasNextInt()) {
                menuOption = scanner.nextInt();

                //Circle
                switch (menuOption)
                {
                    case 1:
                        return 1;
                    case 0:
                        return 0;
                    default:
                        System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                        scanner.next();
                }
            }
            //not digit
            else
            {
                System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                scanner.next();
            }
        } while(true);

    }


    public void printMainDialog()
    {
        System.out.println(ProjectConstant.MSG_WELCOME);

        do
        {
            choiceShape();
        } while (choiceNextStep() == 1);
    }
}
