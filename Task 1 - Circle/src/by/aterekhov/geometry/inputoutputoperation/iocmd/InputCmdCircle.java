package by.aterekhov.geometry.inputoutputoperation.iocmd;

import java.util.ArrayList;
import java.util.Scanner;
import by.aterekhov.geometry.inputoutputoperation.ProjectConstant;

public class InputCmdCircle {

    public static ArrayList<String> inputCmdCircle(Scanner scanner)
    {
        ArrayList<String> inRadials = new ArrayList<String>();
        int inRadialCount = 0;
        String inRadialValue;

        System.out.printf(ProjectConstant.MSG_INPUT_COUNT_RADIAL);

        do {
            //input of some digit
            if (scanner.hasNextInt()) {
                inRadialCount = scanner.nextInt();
                break;
            }
            //not digit
            else
            {
                System.out.println(ProjectConstant.MSG_WRONG_VALUE);
                scanner.next();
            }
        } while(true);

        scanner.nextLine();
        System.out.println(ProjectConstant.MSG_INPUT_RADIAL_LIST);

        for (int i = 0; i < inRadialCount; i++) {
            inRadialValue = scanner.nextLine();
            inRadials.add(inRadialValue);
        }

        return inRadials;
    }
}
