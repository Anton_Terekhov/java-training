package by.aterekhov.geometry.action.circle;

import by.aterekhov.geometry.shape.circle.Circle;

public class CircleAction {

    public static double calculatePerimeter(Circle c)
    {
        return (2 * Math.PI * c.getRadial());
    }

    public static double calculateArea(Circle c)
    {
        return (Math.PI * Math.pow(c.getRadial(), 2));
    }
}
