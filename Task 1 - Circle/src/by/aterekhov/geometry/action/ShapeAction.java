package by.aterekhov.geometry.action;

import by.aterekhov.geometry.shape.circle.Circle;

public abstract class ShapeAction {

    public abstract double calculatePerimeter(Circle c);

    public abstract double calculateArea(Circle c);

}
