package by.aterekhov.geometry.shape.circle;

import by.aterekhov.geometry.shape.Shape;

public class Circle extends Shape {

    private double radial;

    public Circle()
    {
        super("Circle");
    }

    public void setRadial(double inRadial)
    {
        radial = inRadial;
    }

    public double getRadial()
    {
        return radial;
    }

}


