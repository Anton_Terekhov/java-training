package by.aterekhov.geometry.shape;

public abstract class Shape {

    private String name; //Name of shape

    // name by default
    public Shape ()
    {
        name = "Some future shape";
    }

    // name is input parameter
    public Shape (String inName)
    {
        name = inName;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String inName)
    {
        name = inName;
    }

}
