<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources.pagecontent" />
<fmt:setLocale value="en_US" scope="session"/>
<fmt:requestEncoding value="utf-8" />
<%session.setAttribute("locale", "en_US");%>
<html>
<head>
    <link href=”bootstrap/css/bootstrap.css” rel=”stylesheet” type=”text/css” />
    <script type=”text/javascript” src=”bootstrap/js/bootstrap.js”></script>
    <title>Index</title>
</head>
<body>
<jsp:forward page="/jsp/login.jsp"/>
</body>
</html>