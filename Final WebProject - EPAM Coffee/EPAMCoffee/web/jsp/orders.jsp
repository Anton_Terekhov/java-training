<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" />
<%session.setAttribute("page", "orders");%>

<html>
<head>
    <title>Orders</title>


    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-transition.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-scrollspy.js"></script>
    <script src="bootstrap/js/bootstrap-tab.js"></script>
    <script src="bootstrap/js/bootstrap-tooltip.js"></script>
    <script src="bootstrap/js/bootstrap-popover.js"></script>
    <script src="bootstrap/js/bootstrap-button.js"></script>
    <script src="bootstrap/js/bootstrap-collapse.js"></script>
    <script src="bootstrap/js/bootstrap-carousel.js"></script>
    <script src="bootstrap/js/bootstrap-typeahead.js"></script>

    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>

<c:import url="/jsp/common/header_user.jsp" />

<div class="container">

    <h2><fmt:message key="message.order.header.header1"/></h2>

    <c:forEach var="ord" items="${orders}" varStatus="ordstatus">
        <h4><fmt:message key="message.order.header.header2"/> #<c:out value="${ ord.id }" /></h4>
        <h5><fmt:message key="message.order.header.header3"/>: <c:out value="${ ord.orderDT }" /></h5>
        <p>(<fmt:message key="message.order.header.header4"/> - <c:out value="${ ord.orderType.orderTypeNameEng }" />)</p>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><fmt:message key="message.order.dishtype"/></th>
                    <th><fmt:message key="message.order.dish"/></th>
                    <th><fmt:message key="message.order.amount"/></th>
                    <th><fmt:message key="message.order.cost"/></th>
                    <th><fmt:message key="message.order.status"/></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="elem" items="${orderitems}" varStatus="status">
                    <c:if test="${elem.order.id == ord.id}">
                        <tr>
                            <td>
                                <c:choose>
                                    <c:when test="${locale == 'ru_RU'}">
                                        <c:out value="${ elem.menuElement.dish.dishType.getDishTypeNameRus() }" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${ elem.menuElement.dish.dishType.getDishTypeNameEng() }" />
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td>
                                <c:choose>
                                    <c:when test="${locale == 'ru_RU'}">
                                        <c:out value="${ elem.menuElement.dish.getDishNameRus() }" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${ elem.menuElement.dish.getDishNameEng() }" />
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td>
                                <c:out value="${ elem.amount }" />
                            </td>

                            <td>
                                <c:out value="${ elem.menuElement.cost }" />
                            </td>

                            <td>
                                <c:choose>
                                    <c:when test="${locale == 'ru_RU'}">
                                        <c:out value="${ elem.orderItemStatus.getOrderItemStatusNameRus() }" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${ elem.orderItemStatus.getOrderItemStatusNameEng() }" />
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td>
                                <c:if test="${elem.orderItemStatus.getOrderItemStatusNameEng() == 'In Progress'}">
                                    <form name="declineOrderItemForm"  method="POST" action="controller">
                                        <input type="hidden" name="command" value="decline_order_item" />
                                        <button type="submit" class="btn btn-primary btn-md" name="itemid" value="${elem.id}"><fmt:message key="message.order.declineitembutton"/></button>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <hr/>
    </c:forEach>

</div>

<c:import url="/jsp/common/footer.jsp" />

</body>
</html>
