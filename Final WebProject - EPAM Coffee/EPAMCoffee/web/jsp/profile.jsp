<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" />
<%session.setAttribute("page", "profile");%>

<html>

<head>

    <title>Profile</title>

    <link href=”bootstrap/css/bootstrap.css” rel=”stylesheet” type=”text/css” />

    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-transition.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-scrollspy.js"></script>
    <script src="bootstrap/js/bootstrap-tab.js"></script>
    <script src="bootstrap/js/bootstrap-tooltip.js"></script>
    <script src="bootstrap/js/bootstrap-popover.js"></script>
    <script src="bootstrap/js/bootstrap-button.js"></script>
    <script src="bootstrap/js/bootstrap-collapse.js"></script>
    <script src="bootstrap/js/bootstrap-carousel.js"></script>
    <script src="bootstrap/js/bootstrap-typeahead.js"></script>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>

<c:import url="/jsp/common/header_user.jsp" />

    <div class="container">
        <div class="hero-unit">

            <h3><fmt:message key="message.profile.header"/></h3>
            <p><fmt:message key="message.profile.text"/></p>

            <table class="table table-bordered">

                <tbody>
                <tr>
                    <td><fmt:message key="message.profile.firstname"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${locale == 'ru_RU'}">
                                <c:out value="${ user.getUserFirstNameRus() }" />
                            </c:when>
                            <c:otherwise>
                                <c:out value="${ user.getUserFirstNameEng() }" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="message.profile.secondname"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${locale == 'ru_RU'}">
                                <c:out value="${ user.getUserSecondNameRus() }" />
                            </c:when>
                            <c:otherwise>
                                <c:out value="${ user.getUserSecondNameEng() }" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="message.profile.email"/></td>
                    <td>
                        <c:out value="${ user.getUserEmail() }" />
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="message.profile.role"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${locale == 'ru_RU'}">
                                <c:out value="${ user.getUserRole().getRoleNameRus()} (${user.getUserRole().getRoleDescriptionRus() })" />
                            </c:when>
                            <c:otherwise>
                                <c:out value="${ user.getUserRole().getRoleNameEng()} (${user.getUserRole().getRoleDescriptionEng() })" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

<c:import url="/jsp/common/footer.jsp" />

</body>
</html>