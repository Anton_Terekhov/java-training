<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" />
<%session.setAttribute("page", "main");%>

<html>
<head>

    <title>Menu</title>


    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-transition.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-scrollspy.js"></script>
    <script src="bootstrap/js/bootstrap-tab.js"></script>
    <script src="bootstrap/js/bootstrap-tooltip.js"></script>
    <script src="bootstrap/js/bootstrap-popover.js"></script>
    <script src="bootstrap/js/bootstrap-button.js"></script>
    <script src="bootstrap/js/bootstrap-collapse.js"></script>
    <script src="bootstrap/js/bootstrap-carousel.js"></script>
    <script src="bootstrap/js/bootstrap-typeahead.js"></script>


    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>

    <c:import url="/jsp/common/header_user.jsp" />

    <div class="container">

        <div class="hero-unit">
            <h3><fmt:message key="message.main.header.header1"/></h3>
            <p><fmt:message key="message.main.header.header2"/> (<c:out value="${ todaymenu.getMenuDT() }" />)</p>
            <form name="orderForm"  method="POST" action="controller">
            <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><fmt:message key="message.main.table.dishtype"/></th>
                            <th><fmt:message key="message.main.table.dish"/></th>
                            <th><fmt:message key="message.main.table.weight"/></th>
                            <th><fmt:message key="message.main.table.cost"/></th>
                            <th><fmt:message key="message.main.table.amount"/></th>
                            <th><fmt:message key="message.main.table.comment"/></th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set var="lastGroupName" value="dummyValue"/>
                    <c:forEach var="elem" items="${todaymenu.getMenuElements()}" varStatus="status">
                        <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                            <tr>
                                <td>
                                    <c:if test="${group != lastGroupName}">
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishNameRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishNameEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>
                                    <c:out value="${ elem.getDish().getWeight() }" />
                                </td>

                                <td>
                                    <c:out value="${ elem.getCost() }" />
                                </td>

                                <td>
                                    <div class="col-xs-1">
                                        <input type="hidden" name="menuelementid" value="${ elem.getId() }" />
                                        <input class="form-control" name="dishamount" type="number" min="0" max="5" value="0">
                                    </div>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishCommentRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishCommentEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <c:set var="lastGroupName" value="${ group }"/>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
            </table>
            <p>
                <input type="hidden" name="command" value="order" />
                <button type="submit" class="btn btn-primary btn-md"><fmt:message key="message.main.table.orderbutton"/></button>
            </p>
            </form>
        </div>

        <h3><fmt:message key="message.main.header.header3"/></h3>

        <div class="row">
            <div class="span6">
                <h4>
                    <fmt:message key="message.main.week.monday"/>
                    <c:if test="${todaymenu.getMenuDT() == menu2.getMenuDT()}">
                        (<fmt:message key="message.main.week.today"/>)
                    </c:if>
                </h4>
                <p><fmt:message key="message.main.week.text"/> - (<c:out value="${ menu2.getMenuDT() }" />)</p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="message.main.table.dishtype"/></th>
                        <th><fmt:message key="message.main.table.dish"/></th>
                        <th><fmt:message key="message.main.table.cost"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="lastGroupName" value="dummyValue"/>
                    <c:forEach var="elem" items="${menu2.getMenuElements()}" varStatus="status">
                        <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                            <tr>
                                <td>
                                    <c:if test="${group != lastGroupName}">
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishNameRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishNameEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>
                                    <c:out value="${ elem.getCost() }" />
                                </td>

                            </tr>
                            <c:set var="lastGroupName" value="${ group }"/>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div class="span6">
                <h4>
                    <fmt:message key="message.main.week.tuesday"/>
                    <c:if test="${todaymenu.getMenuDT() == menu3.getMenuDT()}">
                        (<fmt:message key="message.main.week.today"/>)
                    </c:if>
                </h4>
                <p><fmt:message key="message.main.week.text"/> - (<c:out value="${ menu3.getMenuDT() }" />)</p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="message.main.table.dishtype"/></th>
                        <th><fmt:message key="message.main.table.dish"/></th>
                        <th><fmt:message key="message.main.table.cost"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="lastGroupName" value="dummyValue"/>
                    <c:forEach var="elem" items="${menu3.getMenuElements()}" varStatus="status">
                        <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                            <tr>
                                <td>
                                    <c:if test="${group != lastGroupName}">
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishNameRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishNameEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>
                                    <c:out value="${ elem.getCost() }" />
                                </td>

                            </tr>
                            <c:set var="lastGroupName" value="${ group }"/>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="span6">
                <h4>
                    <fmt:message key="message.main.week.wednesday"/>
                    <c:if test="${todaymenu.getMenuDT() == menu4.getMenuDT()}">
                        (<fmt:message key="message.main.week.today"/>)
                    </c:if>
                </h4>
                <p><fmt:message key="message.main.week.text"/> - (<c:out value="${ menu4.getMenuDT() }" />)</p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="message.main.table.dishtype"/></th>
                        <th><fmt:message key="message.main.table.dish"/></th>
                        <th><fmt:message key="message.main.table.cost"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="lastGroupName" value="dummyValue"/>
                    <c:forEach var="elem" items="${menu4.getMenuElements()}" varStatus="status">
                        <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                            <tr>
                                <td>
                                    <c:if test="${group != lastGroupName}">
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishNameRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishNameEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>
                                    <c:out value="${ elem.getCost() }" />
                                </td>

                            </tr>
                            <c:set var="lastGroupName" value="${ group }"/>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="span6">
                <h4>
                    <fmt:message key="message.main.week.thursday"/>
                    <c:if test="${todaymenu.getMenuDT() == menu5.getMenuDT()}">
                        (<fmt:message key="message.main.week.today"/>)
                    </c:if>
                </h4>
                <p><fmt:message key="message.main.week.text"/> - (<c:out value="${ menu5.getMenuDT() }" />)</p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="message.main.table.dishtype"/></th>
                        <th><fmt:message key="message.main.table.dish"/></th>
                        <th><fmt:message key="message.main.table.cost"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="lastGroupName" value="dummyValue"/>
                    <c:forEach var="elem" items="${menu5.getMenuElements()}" varStatus="status">
                        <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                            <tr>
                                <td>
                                    <c:if test="${group != lastGroupName}">
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${locale == 'ru_RU'}">
                                            <c:out value="${ elem.getDish().getDishNameRus() }" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${ elem.getDish().getDishNameEng() }" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>
                                    <c:out value="${ elem.getCost() }" />
                                </td>

                            </tr>
                            <c:set var="lastGroupName" value="${ group }"/>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="span6">
                <h4>
                    <fmt:message key="message.main.week.friday"/>
                    <c:if test="${todaymenu.getMenuDT() == menu6.getMenuDT()}">
                        (<fmt:message key="message.main.week.today"/>)
                    </c:if>
                </h4>
                <p><fmt:message key="message.main.week.text"/> - (<c:out value="${ menu6.getMenuDT() }" />)</p>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><fmt:message key="message.main.table.dishtype"/></th>
                            <th><fmt:message key="message.main.table.dish"/></th>
                            <th><fmt:message key="message.main.table.cost"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="lastGroupName" value="dummyValue"/>
                        <c:forEach var="elem" items="${menu6.getMenuElements()}" varStatus="status">
                            <c:forEach var="group" items="${ elem.getDish().getDishType().getDishTypeNameEng() }" varStatus="groupStatus">

                                <tr>
                                    <td>
                                        <c:if test="${group != lastGroupName}">
                                            <c:choose>
                                                <c:when test="${locale == 'ru_RU'}">
                                                    <c:out value="${ elem.getDish().getDishType().getDishTypeNameRus() }" />
                                                </c:when>
                                                <c:otherwise>
                                                    <c:out value="${ elem.getDish().getDishType().getDishTypeNameEng() }" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </td>

                                    <td>
                                        <c:choose>
                                            <c:when test="${locale == 'ru_RU'}">
                                                <c:out value="${ elem.getDish().getDishNameRus() }" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="${ elem.getDish().getDishNameEng() }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </td>

                                    <td>
                                        <c:out value="${ elem.getCost() }" />
                                    </td>

                                </tr>
                                <c:set var="lastGroupName" value="${ group }"/>
                            </c:forEach>
                        </c:forEach>
                        </tbody>
                    </table>
            </div>
            </div>
        </div>
    </div>


    <c:import url="/jsp/common/footer.jsp" />

</body>
</html>