<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" />

<html>
<head>
    <title>Header</title>

    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-transition.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-scrollspy.js"></script>
    <script src="bootstrap/js/bootstrap-tab.js"></script>
    <script src="bootstrap/js/bootstrap-tooltip.js"></script>
    <script src="bootstrap/js/bootstrap-popover.js"></script>
    <script src="bootstrap/js/bootstrap-button.js"></script>
    <script src="bootstrap/js/bootstrap-collapse.js"></script>
    <script src="bootstrap/js/bootstrap-carousel.js"></script>
    <script src="bootstrap/js/bootstrap-typeahead.js"></script>


    <link href=”bootstrap/css/bootstrap.css” rel=”stylesheet” type=”text/css” />
    <style type="text/css">body{padding-top:60px;padding-bottom:40px;margin: 30px;}</style>

</head>
<body>


    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="row">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="/EPAMCoffee/">EPAM Coffee</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><a href="controller?command=menu"> <fmt:message key="message.header.menu.main"/> </a></li>
                            <li><a href="controller?command=history"> <fmt:message key="message.header.menu.orders"/> </a></li>
                            <li><a href="controller?command=profile"> <fmt:message key="message.header.menu.profile"/> </a></li>
                            <li><a href="controller?command=about"> <fmt:message key="message.header.menu.about"/> </a></li>
                            <c:if test="${sessionScope.group == 'Administrator'}">
                                <li><a href="controller?command=administrative"> <fmt:message key="message.header.menu.administrative"/> </a></li>
                            </c:if>

                        </ul>
                        <form class="navbar-form pull-right" name="languageForm"  method="POST" action="controller">
                            <input type="hidden" name="command" value="locale" />
                            <fmt:message key="message.header.languageselect"/>:
                            <select name="language">
                                <c:if test="${locale == 'ru_RU'}">
                                    <option value="ru_RU">Русский</option>
                                </c:if>
                                <option value="en_US">English</option>
                                <c:if test="${locale == 'en_US'}">
                                    <option value="ru_RU">Русский</option>
                                </c:if>
                            </select>
                            <button type="submit" class="btn"><fmt:message key="message.header.languagebutton"/></button>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="nav-collapse collapse">
                        <jsp:useBean id="now" class="java.util.Date"  /> <fmt:message key="message.header.label.today"/>: <fmt:formatDate value="${now}" dateStyle="full"/>
                        <form class="navbar-form pull-right" name="logoutForm"  method="POST" action="controller">
                            <input type="hidden" name="command" value="logout" />
                            <button type="submit" class="btn"><fmt:message key="message.header.menu.logout"/></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
