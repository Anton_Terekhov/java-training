<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" />
<%session.setAttribute("page", "administrative");%>

<html>

<head>

    <title>Administrative</title>

    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-transition.js"></script>
    <script src="bootstrap/js/bootstrap-alert.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>
    <script src="bootstrap/js/bootstrap-dropdown.js"></script>
    <script src="bootstrap/js/bootstrap-scrollspy.js"></script>
    <script src="bootstrap/js/bootstrap-tab.js"></script>
    <script src="bootstrap/js/bootstrap-tooltip.js"></script>
    <script src="bootstrap/js/bootstrap-popover.js"></script>
    <script src="bootstrap/js/bootstrap-button.js"></script>
    <script src="bootstrap/js/bootstrap-collapse.js"></script>
    <script src="bootstrap/js/bootstrap-carousel.js"></script>
    <script src="bootstrap/js/bootstrap-typeahead.js"></script>

    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>

<c:import url="/jsp/common/header_user.jsp" />

    <div class="container">

        <h3><fmt:message key="message.admin.header.header1"/></h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th><fmt:message key="message.admin.table.id"/></th>
                <th><fmt:message key="message.admin.table.firstname"/>(<fmt:message key="message.admin.language.en"/>)</th>
                <th><fmt:message key="message.admin.table.secondname"/>(<fmt:message key="message.admin.language.en"/>)</th>
                <th><fmt:message key="message.admin.table.firstname"/>(<fmt:message key="message.admin.language.ru"/>)</th>
                <th><fmt:message key="message.admin.table.secondname"/>(<fmt:message key="message.admin.language.ru"/>)</th>
                <th><fmt:message key="message.admin.table.email"/></th>
                <th><fmt:message key="message.admin.table.role"/></th>
                <th><fmt:message key="message.admin.table.action"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="elem" items="${users}" varStatus="status">

                <tr>
                    <td><c:out value="${ elem.getId() }" /></td>
                    <td><c:out value="${ elem.getUserFirstNameEng() }" /></td>
                    <td><c:out value="${ elem.getUserSecondNameEng() }" /></td>
                    <td><c:out value="${ elem.getUserFirstNameRus() }" /></td>
                    <td><c:out value="${ elem.getUserSecondNameRus() }" /></td>
                    <td><c:out value="${ elem.getUserEmail() }" /></td>
                    <td>
                        <c:choose>
                            <c:when test="${locale == 'ru_RU'}">
                                <c:out value="${ elem.getUserRole().getRoleNameRus() }" />
                            </c:when>
                            <c:otherwise>
                                <c:out value="${ elem.getUserRole().getRoleNameEng() }" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <form name="deleteUserForm"  method="POST" action="controller">
                            <input type="hidden" name="command" value="delete_user" />
                            <button type="submit" class="btn btn-primary btn-md" name="userid" value="${elem.getId()}"><fmt:message key="message.admin.table.deleteuserbutton"/></button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


    </div>

<c:import url="/jsp/common/footer.jsp" />

</body>
</html>
