package test.by.aterekhov.epamcoffee;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.by.aterekhov.epamcoffee.database.connectionpool.ConnectionPoolTest;

@Suite.SuiteClasses({ConnectionPoolTest.class})
@RunWith(Suite.class)
public class EpamCoffeeSuit {
}
