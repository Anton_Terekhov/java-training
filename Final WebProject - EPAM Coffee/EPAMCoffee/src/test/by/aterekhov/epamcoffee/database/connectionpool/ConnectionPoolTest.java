package test.by.aterekhov.epamcoffee.database.connectionpool;


import by.aterekhov.epamcoffee.database.connectionpool.ConnectionPool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.model.TestTimedOutException;

import java.sql.Connection;

public class ConnectionPoolTest
{

    @Test
    public void getConnectionFromPoolTest()
    {
        Connection cn1 = ConnectionPool.INSTANCE.getConnectionFromPool();
        Connection cn2 = ConnectionPool.INSTANCE.getConnectionFromPool();

        boolean actual = (cn1 == cn2);

        ConnectionPool.INSTANCE.returnConnectionToPool(cn1);
        ConnectionPool.INSTANCE.returnConnectionToPool(cn2);

        Assert.assertFalse(actual);
    }

    @Test
    public void returnConnectionToPool()
    {
        Connection cn1 = ConnectionPool.INSTANCE.getConnectionFromPool();
        Connection cn2 = ConnectionPool.INSTANCE.getConnectionFromPool();
        ConnectionPool.INSTANCE.returnConnectionToPool(cn1);
        ConnectionPool.INSTANCE.returnConnectionToPool(cn2);

        Connection cn3 = ConnectionPool.INSTANCE.getConnectionFromPool();
        Connection cn4 = ConnectionPool.INSTANCE.getConnectionFromPool();

        boolean actual = (((cn1 == cn3)||(cn1 == cn4))&&((cn2 == cn3)||(cn2 == cn4)));

        ConnectionPool.INSTANCE.returnConnectionToPool(cn3);
        ConnectionPool.INSTANCE.returnConnectionToPool(cn4);

        Assert.assertTrue(actual);
    }

}
