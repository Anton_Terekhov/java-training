package by.aterekhov.epamcoffee.database.connectionpool;

import by.aterekhov.epamcoffee.resource.DatabaseManager;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public enum ConnectionPool
{
    INSTANCE;

    private static Logger logger = LogManager.getLogger(ConnectionPool.class);

    private Lock connectionLock = new ReentrantLock();
    private List<Connection> availableConnections = new ArrayList<>();

    ConnectionPool()
    {
        try
        {
            LogManager.getLogger(ConnectionPool.class.getName()).info("Connection pool initialization started.");

            String connString = "jdbc:mysql://" + DatabaseManager.getProperty("db.server.path") + ":" +
                                    DatabaseManager.getProperty("db.server.port") + "/" +
                                    DatabaseManager.getProperty("db.server.dbname")
                                    + "?useUnicode=true&characterEncoding=utf-8&useSSL=false";

            Class.forName(DatabaseManager.getProperty("db.drivername"));

            MysqlDataSource mySQLDS = new MysqlDataSource();
            mySQLDS.setURL(connString);
            mySQLDS.setUser(DatabaseManager.getProperty("db.login.admin.name"));
            mySQLDS.setPassword(DatabaseManager.getProperty("db.login.admin.password"));

            for(int i = 0; i < Integer.parseInt(DatabaseManager.getProperty("db.app.connections.max")); i++)
            {
                Connection conn = DriverManager.getConnection(connString, DatabaseManager.getProperty("db.login.admin.name"), DatabaseManager.getProperty("db.login.admin.password"));
                availableConnections.add(conn);
                LogManager.getLogger(ConnectionPool.class.getName()).info("Connection " + conn.toString() + " added to connection pool.");
            }
            LogManager.getLogger(ConnectionPool.class.getName()).info("Connection pool contains " + DatabaseManager.getProperty("db.app.connections.max") + " connections.");
            LogManager.getLogger(ConnectionPool.class.getName()).info("Connection pool initialization finished successfully.");
        }
        catch (SQLException sqle)
        {
            LogManager.getLogger(ConnectionPool.class.getName()).error("SQL error appeared during connection pool initialization. Details: " + sqle.toString());
        }
        catch (ClassNotFoundException e)
        {
            LogManager.getLogger(ConnectionPool.class.getName()).error("Class Not Found error appeared during connection pool initialization. Details: " + e.toString());
        }
    }

    public Connection getConnectionFromPool()
    {
        Connection connection = null;
        int connectionAmount = 0;
        boolean resultConnectionGet = false;
        try
        {
            while (!resultConnectionGet)
            {
                connectionLock.lock();
                connectionAmount = availableConnections.size();
                if (connectionAmount > 0)
                {
                    connection = (Connection) availableConnections.get(0);
                    availableConnections.remove(0);
                    logger.info("Connection (" + connection.toString() + ") was provided from pool.");
                    resultConnectionGet = true;
                }
                else
                {
                    connectionLock.unlock();
                    TimeUnit.SECONDS.sleep(1);
                }
            }

        }
        catch (InterruptedException ie)
        {
            logger.error("Interrupted exception. Details: " + ie.toString());
        }
        finally
        {
            connectionLock.unlock();
        }

        return connection;
    }

    public void returnConnectionToPool(Connection connection)
    {
        connectionLock.lock();
        try
        {
            availableConnections.add(connection);
            logger.info("Connection (" +connection.toString() + ") was returned to pool.");
        }
        catch (RuntimeException e)
        {
            logger.info("Connection (" +connection.toString() + ") couldn't be returned to pool. Details: " + e.toString());
        }
        finally
        {
            connectionLock.unlock();
        }
    }

}
