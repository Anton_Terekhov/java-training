package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.UserRole;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRoleDAO extends AbstractDAO<UserRole>
{
    private static Logger logger = LogManager.getLogger(UserRoleDAO.class);
    private final static String SQL_SELECT_ALL_USER_ROLES = "SELECT UserRoleId, RoleNameRus, RoleNameEng, RoleDescriptionRus, RoleDescriptionEng FROM AppUserRole;";

    public UserRoleDAO()
    {
        super();
    }

    @Override
    public List<UserRole> retrieveAllEntities()
    {
        List<UserRole> userRoles = new ArrayList<>();
        Statement st = null;
        try {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_USER_ROLES);
            logger.info("Attempt to retrieve all user roles from DB.");
            int k = 0;
            while (resultSet.next())
            {
                UserRole userRole = new UserRole();
                userRole.setId(resultSet.getInt("UserRoleId"));
                userRole.setRoleNameRus(resultSet.getString("RoleNameRus"));
                userRole.setRoleNameEng(resultSet.getString("RoleNameEng"));
                userRole.setRoleDescriptionRus(resultSet.getString("RoleDescriptionRus"));
                userRole.setRoleDescriptionEng(resultSet.getString("RoleDescriptionEng"));
                userRoles.add(userRole);

                k++;
                logger.info("#" + k + " From DB was retrieved user role. Details:");
                logger.info("#" + k + " " + userRole.toString());
            }

            logger.info("All user roles were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return userRoles;
    }

    @Override
    public UserRole findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(UserRole entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(UserRole entity)
    {
        return false;
    }

    @Override
    public UserRole updateEntity(UserRole entity)
    {
        return null;
    }

}
