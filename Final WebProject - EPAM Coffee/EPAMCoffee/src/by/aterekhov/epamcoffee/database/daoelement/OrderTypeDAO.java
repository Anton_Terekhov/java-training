package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.OrderType;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrderTypeDAO extends AbstractDAO<OrderType>
{
    private static Logger logger = LogManager.getLogger(OrderTypeDAO.class);
    private final static String SQL_SELECT_ALL_ORDER_TYPES = "SELECT OrderTypeId, OrderTypeNameRus, OrderTypeNameEng, OrderTypeDescriptionRus, OrderTypeDescriptionEng FROM OrderType;";

    public OrderTypeDAO()
    {
        super();
    }

    @Override
    public List<OrderType> retrieveAllEntities()
    {
        List<OrderType> orderTypes = new ArrayList<>();
        Statement st = null;
        try {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ORDER_TYPES);
            logger.info("Attempt to retrieve all order types from DB.");
            int k = 0;
            while (resultSet.next())
            {
                OrderType orderType = new OrderType();
                orderType.setId(resultSet.getInt("OrderTypeId"));
                orderType.setOrderTypeNameRus(resultSet.getString("OrderTypeNameRus"));
                orderType.setOrderTypeNameEng(resultSet.getString("OrderTypeNameEng"));
                orderType.setOrderTypeDescriptionRus(resultSet.getString("OrderTypeDescriptionRus"));
                orderType.setOrderTypeDescriptionEng(resultSet.getString("OrderTypeDescriptionEng"));
                orderTypes.add(orderType);

                k++;
                logger.info("#" + k + " From DB was retrieved order item status. Details:");
                logger.info("#" + k + " " + orderType.toString());
            }

            logger.info("All order types were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return orderTypes;
    }

    @Override
    public OrderType findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(OrderType entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(OrderType entity)
    {
        return false;
    }

    @Override
    public OrderType updateEntity(OrderType entity)
    {
        return null;
    }

}
