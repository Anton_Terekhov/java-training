package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.Dish;
import by.aterekhov.epamcoffee.data.MenuElement;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MenuDAO extends AbstractDAO<MenuElement>
{

    private static Logger logger = LogManager.getLogger(MenuDAO.class);
    private final static String SQL_SELECT_ALL_MENU = "SELECT MenuCardId, MenuDT, DishId, Cost FROM MenuCard;";
    private final static String SQL_SELECT_RANGE_MENU = "SELECT MenuCardId, MenuDT, DishId, Cost FROM MenuCard WHERE MenuDT>= ? AND MenuDT<= ?;";

    public MenuDAO()
    {
        super();
    }

    @Override
    public List<MenuElement> retrieveAllEntities()
    {
        return null;
    }

    public List<MenuElement> retrieveAllEntities(List<Dish> dishes)
    {
        List<MenuElement> menu = new ArrayList<>();
        Statement st = null;

        try
        {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_MENU);
            logger.info("Attempt to retrieve all menu from DB.");
            int k = parseResultSetMenu(menu, dishes, resultSet);
            logger.info("All menu was retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return menu;
    }

    public List<MenuElement> retrieveEntitiesInRange(List<Dish> dishes, Date startDT, Date endDT)
    {
        List<MenuElement> menu = new ArrayList<>();

        PreparedStatement st = null;

        java.sql.Date sqlStartDT = new java.sql.Date(startDT.getTime());
        java.sql.Date sqlEndDT = new java.sql.Date(endDT.getTime());

        try {
            st = getPreparedStatement(SQL_SELECT_RANGE_MENU);
            st.setDate(1, sqlStartDT);
            st.setDate(2, sqlEndDT);
            ResultSet resultSet = st.executeQuery();
            logger.info("Attempt to retrieve menu from DB in range " + startDT.toString() + " - " + endDT.toString() + ".");

            int k = parseResultSetMenu(menu, dishes, resultSet);

            logger.info("Menu in range was retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return menu;
    }

    private int parseResultSetMenu(List<MenuElement> menu, List<Dish> dishes, ResultSet resultSet) throws SQLException
    {
        int k = 0;

        while (resultSet.next())
        {
            MenuElement menuElement = new MenuElement();
            menuElement.setId(resultSet.getInt("MenuCardId"));
            menuElement.setMenuDT(resultSet.getDate("MenuDT"));

            int dishId = resultSet.getInt("DishId");
            for(Dish dish : dishes)
            {
                if (dish.getId() == dishId)
                {
                    menuElement.setDish(dish);
                    break;
                }
            }

            menuElement.setCost(resultSet.getDouble("Cost"));

            menu.add(menuElement);

            k++;
            logger.info("#" + k + " From DB was retrieved menu element. Details:");
            logger.info("#" + k + " " + menuElement.toString());
        }

        return k;
    }

    @Override
    public MenuElement findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(MenuElement entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(MenuElement entity)
    {
        return false;
    }

    @Override
    public MenuElement updateEntity(MenuElement entity)
    {
        return null;
    }

}
