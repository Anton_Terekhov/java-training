package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.MenuElement;
import by.aterekhov.epamcoffee.data.Order;
import by.aterekhov.epamcoffee.data.OrderItem;
import by.aterekhov.epamcoffee.data.OrderItemStatus;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderItemDAO extends AbstractDAO<OrderItem>
{
    private static Logger logger = LogManager.getLogger(OrderItemDAO.class);
    private final static String SQL_SELECT_ALL_ORDER_ITEMS = "SELECT UserOrderItemId, OrderId, MenuCardId, Amount, OrderItemStatusId FROM UserOrderItem;";
    private final static String SQL_SELECT_ORDER_ITEMS_BY_ORDER = "SELECT UserOrderItemId, OrderId, MenuCardId, Amount, OrderItemStatusId FROM UserOrderItem WHERE OrderId = ?;";
    private final static String SQL_UPDATE_ORDER_ITEM_BY_ORDER = "UPDATE UserOrderItem  SET OrderItemStatusId = (SELECT OrderItemStatusId FROM OrderItemStatus WHERE OrderItemStatusNameEng = 'Declined') WHERE UserOrderItemId = ?;";
    private final static String SQL_INSERT_NEW_ORDER_ITEM = "INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy) VALUES (?, ?, ?, ?, NOW(), 'Admin', NULL, NULL);";


    public OrderItemDAO()
    {
        super();
    }

    public List<OrderItem> retrieveAllEntities(List<MenuElement> menuElements, List<Order> orders, List<OrderItemStatus> orderItemStatuses)
    {
        List<OrderItem> orderItems = new ArrayList<>();
        Statement st = null;
        try {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ORDER_ITEMS);
            logger.info("Attempt to retrieve all order items from DB.");
            int k = 0;
            while (resultSet.next())
            {
                OrderItem orderItem = new OrderItem();
                orderItem.setId(resultSet.getInt("UserOrderItemId"));

                int orderId = resultSet.getInt("OrderId");
                for(Order order : orders)
                {
                    if (order.getId()==orderId)
                    {
                        orderItem.setOrder(order);
                        break;
                    }
                }

                int menuElementId = resultSet.getInt("MenuCardId");
                for(MenuElement menuElement : menuElements)
                {
                    if (menuElement.getId()==menuElementId)
                    {
                        orderItem.setMenuElement(menuElement);
                        break;
                    }
                }

                orderItem.setAmount(resultSet.getDouble("Amount"));

                int orderItemStatusId = resultSet.getInt("OrderItemStatusId");
                for(OrderItemStatus orderItemStatus : orderItemStatuses)
                {
                    if (orderItemStatus.getId() == orderItemStatusId)
                    {
                        orderItem.setOrderItemStatus(orderItemStatus);
                        break;
                    }
                }

                orderItems.add(orderItem);

                k++;
                logger.info("#" + k + " From DB was retrieved order item. Details:");
                logger.info("#" + k + " " + orderItem.toString());
            }

            logger.info("All order items were retrieved. Amount is " + k + ".");

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }
        return orderItems;
    }

    @Override
    public List<OrderItem> retrieveAllEntities()
    {
        return null;
    }

    public List<OrderItem> findEntityByOrder(List<MenuElement> menuElements, Order order, List<OrderItemStatus> orderItemStatuses)
    {
        List<OrderItem> orderItems = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = getPreparedStatement(SQL_SELECT_ORDER_ITEMS_BY_ORDER);
            st.setInt(1, order.getId());
            ResultSet resultSet = st.executeQuery();
            logger.info("Attempt to retrieve all order items for order [ " + order.toString() + " ] from DB.");
            int k = 0;
            while (resultSet.next())
            {
                OrderItem orderItem = new OrderItem();
                orderItem.setId(resultSet.getInt("UserOrderItemId"));
                orderItem.setOrder(order);

                int menuElementId = resultSet.getInt("MenuCardId");
                for(MenuElement menuElement : menuElements)
                {
                    if (menuElement.getId()==menuElementId)
                    {
                        orderItem.setMenuElement(menuElement);
                        break;
                    }
                }

                orderItem.setAmount(resultSet.getDouble("Amount"));

                int orderItemStatusId = resultSet.getInt("OrderItemStatusId");
                for(OrderItemStatus orderItemStatus : orderItemStatuses)
                {
                    if (orderItemStatus.getId() == orderItemStatusId)
                    {
                        orderItem.setOrderItemStatus(orderItemStatus);
                        break;
                    }
                }

                orderItems.add(orderItem);

                k++;
                logger.info("#" + k + " From DB was retrieved order item. Details:");
                logger.info("#" + k + " " + orderItem.toString());
            }

            logger.info(orderItems.toString());

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }
        return orderItems;
    }

    @Override
    public OrderItem findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(OrderItem entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(OrderItem entity)
    {
        return false;
    }

    public int addEntity(int orderId, int menuElementId, double amount, int status)
    {
        PreparedStatement st = null;
        int newOrderItemId = 0;

        try
        {
            st = getPreparedStatement(SQL_INSERT_NEW_ORDER_ITEM);
            st.setInt(1, orderId);
            st.setInt(2, menuElementId);
            st.setDouble(3, amount);
            st.setInt(4, status);

            logger.info("Attempt to create new order item in DB for order with id = " + orderId + ".");
            int affectedRows  = st.executeUpdate();
            if (affectedRows == 0)
            {
                logger.error("Order Item creation was failed. No inserted records.");
            }
            else
            {
                logger.info("New order item was created.");
            }

            try (ResultSet generatedKeys = st.getGeneratedKeys()) {
                if (generatedKeys.next())
                {
                    newOrderItemId = generatedKeys.getInt(1);
                    logger.info("ID of new order item = " + newOrderItemId +".");
                }
                else
                {
                    logger.error("New order item id can't be retrieved.");
                }
            }

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return newOrderItemId;
    }

    @Override
    public OrderItem updateEntity(OrderItem entity)
    {
        return null;
    }

    public boolean declineOrderItemById (int id)
    {
        PreparedStatement st = null;
        boolean updateResult = false;

        try
        {
            st = getPreparedStatement(SQL_UPDATE_ORDER_ITEM_BY_ORDER);
            st.setInt(1, id);
            logger.info("Attempt to update order item (decline) in DB for order item with id = " + id + ".");
            int affectedRows = st.executeUpdate();

            if (affectedRows == 0)
            {
                logger.error("Decline of Order Item was failed, no rows affected.");
            }
            else
            {
                logger.info("Order Item with id = " + id + " was declined.");
                updateResult = true;
            }
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return updateResult;
    }

}
