package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.data.UserRole;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO extends AbstractDAO<User>
{
    private static Logger logger = LogManager.getLogger(UserDAO.class);
    private final static String SQL_SELECT_ALL_USERS = "SELECT UserId, UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId FROM AppUser;";
    private final static String SQL_SELECT_USER_BY_ID = "SELECT UserId, UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId FROM AppUser WHERE UserId = ?;";
    private final static String SQL_SELECT_USER_BY_EMAIL = "SELECT UserId, UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId FROM AppUser WHERE UserEmail = ?;";
    private final static String SQL_DELETE_USER_BY_ID = "DELETE FROM AppUser WHERE UserId = ?;";

    public UserDAO()
    {
        super();
    }

    @Override
    public List<User> retrieveAllEntities()
    {
        return null;
    }

    public List<User> retrieveAllEntities(List<UserRole> userRoles)
    {
        List<User> users = new ArrayList<>();
        Statement st = null;
        try {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_USERS);
            logger.info("Attempt to retrieve all users from DB.");
            int k = 0;
            while (resultSet.next())
            {
                User user = new User();
                user.setId(resultSet.getInt("UserId"));
                user.setUserFirstNameRus(resultSet.getString("UserFirstNameRus"));
                user.setUserFirstNameEng(resultSet.getString("UserFirstNameEng"));
                user.setUserSecondNameRus(resultSet.getString("UserSecondNameRus"));
                user.setUserSecondNameEng(resultSet.getString("UserSecondNameEng"));
                user.setUserEmail(resultSet.getString("UserEmail"));
                user.setUserPassword(resultSet.getString("UserPassword"));

                int roleId = resultSet.getInt("UserRoleId");

                for(UserRole userRole : userRoles)
                {
                    if (userRole.getId()==roleId)
                    {
                        user.setUserRole(userRole);
                        break;
                    }
                }

                users.add(user);

                k++;
                logger.info("#" + k + " From DB was retrieved user. Details:");
                logger.info("#" + k + " " + user.toString());
            }

            logger.info("All order types were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return users;
    }

    public User findEntityByEmail(List<UserRole> userRoles, String email)
    {
        PreparedStatement st = null;
        User user = null;

        try {
            st = getPreparedStatement(SQL_SELECT_USER_BY_EMAIL);
            st.setString(1, email);
            ResultSet resultSet = st.executeQuery();
            logger.info("Attempt to retrieve user by email from DB.");

            resultSet.next();

            user = new User();
            user.setId(resultSet.getInt("UserId"));
            user.setUserFirstNameRus(resultSet.getString("UserFirstNameRus"));
            user.setUserFirstNameEng(resultSet.getString("UserFirstNameEng"));
            user.setUserSecondNameRus(resultSet.getString("UserSecondNameRus"));
            user.setUserSecondNameEng(resultSet.getString("UserSecondNameEng"));
            user.setUserEmail(resultSet.getString("UserEmail"));
            user.setUserPassword(resultSet.getString("UserPassword"));

            int roleId = resultSet.getInt("UserRoleId");

            for(UserRole userRole : userRoles)
            {
                if (userRole.getId()==roleId)
                {
                    user.setUserRole(userRole);
                    break;
                }
            }

            logger.info("From DB was retrieved user. Details:");
            logger.info(user.toString());

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return user;
    }

    public String findUserPasswordByEmail(String email)
    {
        PreparedStatement st = null;
        String password = null;

        try
        {
            st = getPreparedStatement(SQL_SELECT_USER_BY_EMAIL);
            st.setString(1, email);
            ResultSet resultSet = st.executeQuery();

            logger.info("Attempt to retrieve user password by email from DB.");
            resultSet.next();

            password = resultSet.getString("UserPassword");

            logger.info("Password  of " + email + " user was retrieved from DB.");

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return password;
    }

    @Override
    public User findEntityById(int id)
    {
        return null;
    }


    public User findEntityById(List<UserRole> userRoles, int id)
    {
        PreparedStatement st = null;
        User user = null;

        try {
            st = getPreparedStatement(SQL_SELECT_USER_BY_ID);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            logger.info("Attempt to retrieve user by id from DB.");

            resultSet.next();

            user = new User();
            user.setId(resultSet.getInt("UserId"));
            user.setUserFirstNameRus(resultSet.getString("UserFirstNameRus"));
            user.setUserFirstNameEng(resultSet.getString("UserFirstNameEng"));
            user.setUserSecondNameRus(resultSet.getString("UserSecondNameRus"));
            user.setUserSecondNameEng(resultSet.getString("UserSecondNameEng"));
            user.setUserEmail(resultSet.getString("UserEmail"));
            user.setUserPassword(resultSet.getString("UserPassword"));

            int roleId = resultSet.getInt("UserRoleId");

            for(UserRole userRole : userRoles)
            {
                if (userRole.getId()==roleId)
                {
                    user.setUserRole(userRole);
                    break;
                }
            }

            logger.info("From DB was retrieved user. Details:");
            logger.info(user.toString());

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return user;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        PreparedStatement st = null;
        boolean deleteResult = false;

        try
        {
            st = getPreparedStatement(SQL_DELETE_USER_BY_ID);
            st.setInt(1, id);

            logger.info("Attempt to delete user in DB with id = " + id + ".");
            int affectedRows = st.executeUpdate();

            if (affectedRows == 0)
            {
                logger.error("Deleting user failed, no rows affected.");
            }
            else
            {
                logger.info("User with id = " + id + " was deleted.");
                deleteResult = true;
            }
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return deleteResult;
    }

    @Override
    public boolean deleteEntity(User entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(User entity)
    {
        return false;
    }

    @Override
    public User updateEntity(User entity)
    {
        return null;
    }

}

