package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.DishType;

import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

public class DishTypeDAO extends AbstractDAO<DishType>
{
    private static Logger logger = LogManager.getLogger(DishTypeDAO.class);
    private final static String SQL_SELECT_ALL_DISH_TYPES = "SELECT DishTypeId, DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder FROM DishType;";

    public DishTypeDAO()
    {
        super();
    }

    @Override
    public List<DishType> retrieveAllEntities()
    {
        List<DishType> dishTypes = new ArrayList<>();
        Statement st = null;
        try
        {

            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_DISH_TYPES);
            logger.info("Attempt to retrieve all dish types from DB.");
            int k = 0;
            while (resultSet.next())
            {
                DishType dishType = new DishType();
                dishType.setId(resultSet.getInt("DishTypeId"));
                dishType.setDishTypeNameRus(resultSet.getString("DishTypeNameRus"));
                dishType.setDishTypeNameEng(resultSet.getString("DishTypeNameEng"));
                dishType.setDishTypeSortOrder(resultSet.getInt("DishTypeSortOrder"));
                dishTypes.add(dishType);

                k++;
                logger.info("#" + k + " From DB was retrieved dish type. Details:");
                logger.info("#" + k + " " + dishType.toString());
            }
            logger.info("All dish types were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return dishTypes;
    }

    @Override
    public DishType findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(DishType entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(DishType entity)
    {
        return false;
    }

    @Override
    public DishType updateEntity(DishType entity)
    {
        return null;
    }

}
