package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.Dish;
import by.aterekhov.epamcoffee.data.DishType;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DishDAO extends AbstractDAO<Dish>
{
    private static Logger logger = LogManager.getLogger(DishDAO.class);
    private final static String SQL_SELECT_ALL_DISHES = "SELECT DishId, DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng FROM Dish;";

    public DishDAO()
    {
        super();
    }

    @Override
    public List<Dish> retrieveAllEntities()
    {
        return null;
    }

    public List<Dish> retrieveAllEntities(List<DishType> dishTypes)
    {

        List<Dish> dishes = new ArrayList<>();
        Statement st = null;

        try
        {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_DISHES);
            logger.info("Attempt to retrieve all dishes from DB.");
            int k = 0;
            while (resultSet.next())
            {
                Dish dish = new Dish();
                dish.setId(resultSet.getInt("DishId"));
                dish.setDishNameRus(resultSet.getString("DishNameRus"));
                dish.setDishNameEng(resultSet.getString("DishNameEng"));

                int dishTypeId = resultSet.getInt("DishTypeId");
                for(DishType dishType : dishTypes)
                {
                    if (dishType.getId()==dishTypeId)
                    {
                        dish.setDishType(dishType);
                        break;
                    }
                }

                dish.setWeight(resultSet.getDouble("Weight"));
                dish.setDishCommentRus(resultSet.getString("DishCommentRus"));
                dish.setDishCommentEng(resultSet.getString("DishCommentEng"));
                dishes.add(dish);

                k++;
                logger.info("#" + k + " From DB was retrieved dish. Details:");
                logger.info("#" + k + " " + dish.toString());
            }
            logger.info("All dished were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e.toString());
        }
        finally
        {
            closeStatement(st);
        }
        return dishes;
    }

    @Override
    public Dish findEntityById(int id) {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id) {
        return false;
    }

    @Override
    public boolean deleteEntity(Dish entity) {
        return false;
    }

    @Override
    public boolean addEntity(Dish entity) {
        return false;
    }

    @Override
    public Dish updateEntity(Dish entity) {
        return null;
    }

}
