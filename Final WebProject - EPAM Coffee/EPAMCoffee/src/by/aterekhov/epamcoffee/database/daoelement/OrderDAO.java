package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.Order;
import by.aterekhov.epamcoffee.data.OrderType;
import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

public class OrderDAO extends AbstractDAO<Order>
{
    private static Logger logger = LogManager.getLogger(OrderDAO.class);
    private final static String SQL_SELECT_ALL_ORDERS = "SELECT OrderId, OrderDT, UserId, OrderTypeId, OrderComment FROM UserOrder;";
    private final static String SQL_SELECT_ORDER_BY_USER = "SELECT OrderId, OrderDT, UserId, OrderTypeId, OrderComment FROM UserOrder WHERE UserId = ?;";
    private final static String SQL_INSERT_NEW_ORDER = "INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy) VALUES (CAST(? AS DATE), ?, ?, ?, NOW(), 'Admin', NULL, NULL);";

    public OrderDAO()
    {
        super();
    }

    public List<Order> retrieveAllEntities(List<OrderType> orderTypes, List<User> users)
    {
        List<Order> orders = new ArrayList<>();
        Statement st = null;
        try
        {
            st =getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ORDERS);
            logger.info("Attempt to retrieve all orders from DB.");
            int k = 0;
            while (resultSet.next())
            {
                Order order = new Order();
                order.setId(resultSet.getInt("OrderId"));
                order.setOrderDT(resultSet.getDate("OrderDT"));

                int orderUserId = resultSet.getInt("UserId");
                for(User user : users)
                {
                    if (user.getId()==orderUserId)
                    {
                        order.setUser(user);
                        break;
                    }
                }

                int orderTypeId = resultSet.getInt("OrderTypeId");
                for(OrderType orderType : orderTypes)
                {
                    if (orderType.getId()==orderTypeId)
                    {
                        order.setOrderType(orderType);
                        break;
                    }
                }

                order.setOrderComment(resultSet.getString("OrderComment"));
                orders.add(order);

                k++;
                logger.info("#" + k + " From DB was retrieved order. Details:");
                logger.info("#" + k + " " + order.toString());
            }

            logger.info("All orders were retrieved. Amount is " + k + ".");

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }
        return orders;
    }

    @Override
    public List<Order> retrieveAllEntities()
    {
        return null;
    }

    public List<Order> findEntityByUser(List<OrderType> orderTypes, User user)
    {
        List<Order> orders = new ArrayList<>();
        PreparedStatement st = null;
        try
        {
            st = getPreparedStatement(SQL_SELECT_ORDER_BY_USER);
            st.setInt(1,user.getId());
            ResultSet resultSet = st.executeQuery();
            logger.info("Attempt to retrieve all orders from DB for user " + user.getUserEmail() + ".");
            int k = 0;
            while (resultSet.next())
            {
                Order order = new Order();
                order.setId(resultSet.getInt("OrderId"));
                order.setOrderDT(resultSet.getDate("OrderDT"));
                order.setUser(user);

                int orderTypeId = resultSet.getInt("OrderTypeId");
                for(OrderType orderType : orderTypes)
                {
                    if (orderType.getId()==orderTypeId)
                    {
                        order.setOrderType(orderType);
                        break;
                    }
                }

                order.setOrderComment(resultSet.getString("OrderComment"));
                orders.add(order);

                k++;
                logger.info("#" + k + " From DB was retrieved order. Details:");
                logger.info("#" + k + " " + order.toString());
            }

            logger.info("All user's orders were retrieved. Amount is " + k + ".");

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }
        return orders;
    }

    @Override
    public Order findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(Order entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(Order entity)
    {
        return false;
    }

    public int addEntity(java.util.Date date, int userId, int orderTypeId, String comments)
    {
        PreparedStatement st = null;
        java.sql.Date sqlOrderDT = new java.sql.Date(date.getTime());
        int newOrderId = 0;

        try {
            st = getPreparedStatement(SQL_INSERT_NEW_ORDER);
            st.setDate(1, sqlOrderDT);
            st.setInt(2, userId);
            st.setInt(3,orderTypeId);
            st.setString(4, comments);

            logger.info("Attempt to create new order in DB for user with id = " + userId + ".");
            int affectedRows  = st.executeUpdate();
            if (affectedRows == 0)
            {
                logger.error("Order creation was failed. No inserted records.");
            }
            else
            {
                logger.info("New order was created.");
            }

            try (ResultSet generatedKeys = st.getGeneratedKeys())
            {
                if (generatedKeys.next())
                {
                    newOrderId = generatedKeys.getInt(1);
                    logger.info("ID of new order = " + newOrderId +".");

                }
                else
                {
                    logger.error("New order id can't be retrieved.");
                }
            }

        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }

        return newOrderId;
    }

    @Override
    public Order updateEntity(Order entity)
    {
        return null;
    }

}
