package by.aterekhov.epamcoffee.database.daoelement;

import by.aterekhov.epamcoffee.data.OrderItemStatus;
import by.aterekhov.epamcoffee.database.AbstractDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrderItemStatusDAO extends AbstractDAO<OrderItemStatus>
{
    private static Logger logger = LogManager.getLogger(OrderItemStatusDAO.class);
    private final static String SQL_SELECT_ALL_ORDER_ITEM_STATUSES = "SELECT OrderItemStatusId, OrderItemStatusNameRus, OrderItemStatusNameEng, OrderItemStatusDescriptionRus, OrderItemStatusDescriptionEng FROM OrderItemStatus;";

    public OrderItemStatusDAO()
    {
        super();
    }

    @Override
    public List<OrderItemStatus> retrieveAllEntities()
    {
        List<OrderItemStatus> orderItemStatuses = new ArrayList<>();
        Statement st = null;
        try
        {
            st = getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ORDER_ITEM_STATUSES);
            logger.info("Attempt to retrieve all order item statuses from DB.");
            int k = 0;
            while (resultSet.next())
            {
                OrderItemStatus orderItemStatus = new OrderItemStatus();
                orderItemStatus.setId(resultSet.getInt("OrderItemStatusId"));
                orderItemStatus.setOrderItemStatusNameRus(resultSet.getString("OrderItemStatusNameRus"));
                orderItemStatus.setOrderItemStatusNameEng(resultSet.getString("OrderItemStatusNameEng"));
                orderItemStatus.setOrderItemStatusDescriptionRus(resultSet.getString("OrderItemStatusDescriptionRus"));
                orderItemStatus.setOrderItemStatusDescriptionEng(resultSet.getString("OrderItemStatusDescriptionEng"));
                orderItemStatuses.add(orderItemStatus);

                k++;
                logger.info("#" + k + " From DB was retrieved order item status. Details:");
                logger.info("#" + k + " " + orderItemStatus.toString());
            }

            logger.info("All order item statuses were retrieved. Amount is " + k + ".");
        }
        catch (SQLException e)
        {
            logger.error("SQL exception (request or table failed): " + e);
        }
        finally
        {
            closeStatement(st);
        }
        return orderItemStatuses;
    }

    @Override
    public OrderItemStatus findEntityById(int id)
    {
        return null;
    }

    @Override
    public boolean deleteEntityById(int id)
    {
        return false;
    }

    @Override
    public boolean deleteEntity(OrderItemStatus entity)
    {
        return false;
    }

    @Override
    public boolean addEntity(OrderItemStatus entity)
    {
        return false;
    }

    @Override
    public OrderItemStatus updateEntity(OrderItemStatus entity)
    {
        return null;
    }

}
