package by.aterekhov.epamcoffee.database;

import by.aterekhov.epamcoffee.data.Entity;
import by.aterekhov.epamcoffee.database.connectionpool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class AbstractDAO <T extends Entity>
{
    private Connection connection;
    private static Logger logger = LogManager.getLogger(AbstractDAO.class);

    public AbstractDAO()
    {
        connection = ConnectionPool.INSTANCE.getConnectionFromPool();
    }

    public abstract List<T> retrieveAllEntities();
    public abstract T findEntityById(int id);
    public abstract boolean deleteEntityById(int id);
    public abstract boolean deleteEntity(T entity);
    public abstract boolean addEntity(T entity);
    public abstract T updateEntity(T entity);

    public void returnConnectionToPool()
    {
        ConnectionPool.INSTANCE.returnConnectionToPool(connection);
    }

    public PreparedStatement getPreparedStatement(String sql)
    {
        PreparedStatement ps = null;
        try
        {
            ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        }
        catch (SQLException e)
        {
            logger.error("SQL exception during getting of Prepared Statement. Details: " + e.toString());
        }

        return ps;
    }

    public Statement getStatement()
    {
        Statement ps = null;
        try
        {
            ps = connection.createStatement();
        }
        catch (SQLException e)
        {
            logger.error("SQL exception during getting of Prepared Statement. Details: " + e.toString());
        }

        return ps;
    }

    public void closeStatement(PreparedStatement ps)
    {
        if (ps != null)
        {
            try
            {
                ps.close();
            }
            catch (SQLException e)
            {
                logger.error("SQL exception during closing of Prepared Statement. Details: " + e.toString());
            }
        }
    }

    public void closeStatement(Statement ps)
    {
        if (ps != null)
        {
            try
            {
                ps.close();
            }
            catch (SQLException e)
            {
                logger.error("SQL exception during closing of Statement. Details: " + e.toString());
            }
        }
    }
}
