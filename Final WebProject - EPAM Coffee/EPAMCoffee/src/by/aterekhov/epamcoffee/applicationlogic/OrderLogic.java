package by.aterekhov.epamcoffee.applicationlogic;

import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.shareapplicationobject.ShareInternalObject;
import by.aterekhov.epamcoffee.shareapplicationobject.ShareMenu;
import by.aterekhov.epamcoffee.data.Order;
import by.aterekhov.epamcoffee.data.OrderItem;
import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.database.daoelement.OrderDAO;
import by.aterekhov.epamcoffee.database.daoelement.OrderItemDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderLogic
{
    private static Logger logger = LogManager.getLogger(OrderLogic.class);

    public static List<Order> retrieveAllOrders(User user) throws BusinessLogicException
    {
        if(user == null)
        {
            logger.error("Attempt to retrieve orders of empty(null) user.");
            throw new BusinessLogicException("Error during get user information.");
        }

        List<Order> orders;

        logger.info("Attempt to retrieve all orders of user " + user.getUserEmail());

        OrderDAO orderDAO = new OrderDAO();
        orders = orderDAO.findEntityByUser(ShareInternalObject.APPLICATION.retrieveOrderTypes(),user);
        orderDAO.returnConnectionToPool();

        return orders;
    }

    public static List<OrderItem> retrieveAllOrderItems(List<Order> orders) throws BusinessLogicException
    {
        if(orders == null)
        {
            logger.error("Attempt to retrieve all order item for empty(null) order.");
            throw new BusinessLogicException("Order list is empty, retrieving of order items can not be done.");
        }

        List<OrderItem> orderItems = new ArrayList<>();

        logger.info("Attempt to retrieve all order items for all user's order.");

        OrderItemDAO orderItemDAO = new OrderItemDAO();
        for (Order order : orders)
        {
            logger.info("Retrieving of data for " + order.toString());
            orderItems.addAll(orderItemDAO.findEntityByOrder(ShareMenu.MENU.retrieveWholeMenu(), order, ShareInternalObject.APPLICATION.retrieveOrderItemStatuses()));
        }
        orderItemDAO.returnConnectionToPool();

        return orderItems;
    }

    public static void declineOrderItemById(int id) throws BusinessLogicException
    {
        logger.info("Attempt to remove order item with id = " + id + ".");

        OrderItemDAO orderItemDAO = new OrderItemDAO();
        boolean declineResult = orderItemDAO.declineOrderItemById(id);
        orderItemDAO.returnConnectionToPool();

        if (!declineResult)
        {
            logger.error("Attempt to remove order item was failed.");
            throw new BusinessLogicException("Removing of order item was failed.");
        }
        else
        {
            logger.info("Attempt to remove order item with id = " + id + " was successful.");
        }
    }

    public static int createOrder(int userId, int orderTypeId, String comments) throws BusinessLogicException
    {
        logger.info("Attempt to create a new user's order.");

        OrderDAO orderDAO = new OrderDAO();
        Date currentDate = new Date();
        int orderId = orderDAO.addEntity(currentDate, userId, orderTypeId, comments);
        orderDAO.returnConnectionToPool();

        if (orderId == 0)
        {
            logger.error("Attempt to create a new order was failed.");
            throw new BusinessLogicException("Creation of order was failed.");
        }
        else
        {
            logger.info("Attempt to create a new order was successful. Order id = " + orderId + ".");
        }

        return orderId;
    }

    public static void createOrderItems(int orderId, int menuElementId, double dishAmount) throws BusinessLogicException
    {
        logger.info("Attempt to add order item to order with id = " + orderId + ".");

        OrderItemDAO orderItemDAO = new OrderItemDAO();
        int orderItemId = orderItemDAO.addEntity(orderId, menuElementId, dishAmount, 1);
        orderItemDAO.returnConnectionToPool();

        if (orderItemId == 0)
        {
            logger.error("Attempt to add order item to order was failed.");
            throw new BusinessLogicException("Creation of order item was failed.");
        }
        else
        {
            logger.info("Attempt to add order item to order was successful. Order item id = " + orderItemId + ".");
        }

    }
}
