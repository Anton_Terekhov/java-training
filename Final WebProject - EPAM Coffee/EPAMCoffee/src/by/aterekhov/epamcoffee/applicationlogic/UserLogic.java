package by.aterekhov.epamcoffee.applicationlogic;

import by.aterekhov.epamcoffee.database.daoelement.UserDAO;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.shareapplicationobject.ShareInternalObject;
import by.aterekhov.epamcoffee.data.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserLogic
{
    private static Logger logger = LogManager.getLogger(UserLogic.class);

    public static boolean checkLogin(String enterLogin, String enterPass)
    {
        if(enterLogin.equals("")||enterPass.equals(""))
        {
            logger.info("User entered empty login or password, check is impossible.");
            return false;
        }

        logger.info("Attempt of user authentication.");

        boolean loginResult = false;

        UserDAO userDAO = new UserDAO();
        String userPassword = userDAO.findUserPasswordByEmail(enterLogin);
        userDAO.returnConnectionToPool();

        if(userPassword!=null&&userPassword.equals(enterPass))
        {
            loginResult = true;
            logger.info("User authentication was successful");
        }
        else
        {
            logger.info("Entered user login or password is wrong.");
        }

        return loginResult;
    }

    public static User getUser(String email) throws BusinessLogicException
    {
        User user;

        logger.info("Attempt to retrieve user " + email + ".");

        UserDAO userDAO = new UserDAO();
        user = userDAO.findEntityByEmail(ShareInternalObject.APPLICATION.retrieveUserRoles(), email);
        userDAO.returnConnectionToPool();

        if(user == null)
        {
            logger.error("Attempt to retrieve user " + email + " was failed.");
        }
        else
        {
            logger.info("Retrieving of user " + email + " was successful.");
        }

        return user;
    }

    public static List<User> getAllUsers() throws BusinessLogicException
    {
        List<User> users;

        UserDAO userDAO = new UserDAO();
        users = userDAO.retrieveAllEntities(ShareInternalObject.APPLICATION.retrieveUserRoles());
        userDAO.returnConnectionToPool();

        if(users == null)
        {
            logger.info("No users were retrieved.");
        }
        else
        {
            logger.info("Retrieving of all users was successful.");
        }

        return users;
    }

    public static void deleteUserById(int id) throws BusinessLogicException
    {
        logger.info("Attempt to delete user with id = " + id);
        UserDAO userDAO = new UserDAO();
        boolean deleteResult = userDAO.deleteEntityById(id);
        userDAO.returnConnectionToPool();

        if(!deleteResult)
        {
            logger.error("User removing was failed");
            throw new BusinessLogicException("Error during user removing.");
        }
        else
        {
            logger.info("User with id = " + id + " was deleted.");
        }
    }
}
