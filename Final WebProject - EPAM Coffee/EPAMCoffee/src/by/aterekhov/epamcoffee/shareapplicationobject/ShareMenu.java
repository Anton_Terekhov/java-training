package by.aterekhov.epamcoffee.shareapplicationobject;

import by.aterekhov.epamcoffee.data.DayMenu;
import by.aterekhov.epamcoffee.data.Dish;
import by.aterekhov.epamcoffee.data.DishType;
import by.aterekhov.epamcoffee.data.MenuElement;
import by.aterekhov.epamcoffee.database.daoelement.DishDAO;
import by.aterekhov.epamcoffee.database.daoelement.DishTypeDAO;
import by.aterekhov.epamcoffee.database.daoelement.MenuDAO;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public enum ShareMenu
{
    MENU;

    private List<DishType> dishTypes;
    private List<Dish> dishes;
    private List<MenuElement> weekMenu;
    private List<MenuElement> wholeMenu;

    private static Logger logger = LogManager.getLogger(ShareMenu.class);

    ShareMenu()
    {
        DishTypeDAO dishTypeDAO = new DishTypeDAO();
        dishTypes = dishTypeDAO.retrieveAllEntities();
        dishTypeDAO.returnConnectionToPool();
        LogManager.getLogger(ShareMenu.class).info("Dish Types are built. Details: " + dishTypes.toString());

        DishDAO dishDAO = new DishDAO();
        dishes = dishDAO.retrieveAllEntities(dishTypes);
        dishDAO.returnConnectionToPool();
        LogManager.getLogger(ShareMenu.class).info("Dishes are built. Details: " + dishes.toString());

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);

        Date weekStart = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, 6);
        Date weekEnd = c.getTime();

        MenuDAO menuDAO = new MenuDAO();
        weekMenu = menuDAO.retrieveEntitiesInRange(dishes, weekStart, weekEnd);

        LogManager.getLogger(ShareMenu.class).info("Week mune is built. Details: " + weekMenu.toString());

        wholeMenu = menuDAO.retrieveAllEntities(dishes);

        LogManager.getLogger(ShareMenu.class).info("Whole menu is built. Details: " + wholeMenu.toString());

        menuDAO.returnConnectionToPool();
    }


    public synchronized List<DishType> retrieveDishTypes() throws BusinessLogicException
    {
        if(dishTypes==null)
        {
            logger.error("Attempt to retrieve empty list of dish types.");
            throw new BusinessLogicException("Dish types aren't defined (empty).");
        }

        logger.info("Dish types were provided.");
        return dishTypes;
    }

    public synchronized void buildDishTypes()
    {
        DishTypeDAO dishTypeDAO = new DishTypeDAO();
        dishTypes = dishTypeDAO.retrieveAllEntities();
        dishTypeDAO.returnConnectionToPool();
        logger.info("Dish Types are built. Details: " + dishTypes.toString());
    }

    public synchronized List<Dish> retrieveDishes() throws BusinessLogicException
    {
        if(dishes==null)
        {
            logger.error("Attempt to retrieve empty list of dishes.");
            throw new BusinessLogicException("Dishes aren't defined (empty).");
        }

        logger.info("Dishes were provided.");
        return dishes;
    }

    public synchronized void buildDishes()
    {
        DishDAO dishDAO = new DishDAO();
        dishes = dishDAO.retrieveAllEntities(dishTypes);
        dishDAO.returnConnectionToPool();
        logger.info("Dishes are built. Details: " + dishes.toString());
    }

    public synchronized List<MenuElement> retrieveWeekMenu() throws BusinessLogicException
    {
        if(weekMenu==null)
        {
            logger.error("Attempt to retrieve empty week menu.");
            throw new BusinessLogicException("Week menu is empty.");
        }

        return weekMenu;
    }

    public synchronized Map<Integer, DayMenu> retrieveWeekMenuByDay() throws BusinessLogicException
    {
        if(weekMenu==null)
        {
            logger.error("Attempt to retrieve empty week menu by day.");
            throw new BusinessLogicException("Week menu is empty.");
        }

        Map<Integer, DayMenu> weekMenuByDay = new HashMap<>();
        DayMenu menu1 = new DayMenu();
        DayMenu menu2 = new DayMenu();
        DayMenu menu3 = new DayMenu();
        DayMenu menu4 = new DayMenu();
        DayMenu menu5 = new DayMenu();
        DayMenu menu6 = new DayMenu();

        for (MenuElement element:weekMenu)
        {
            element.getDayOfWeek();
            switch (element.getDayOfWeek())
            {
                case 1:
                    menu1.addMenuElement(element);
                    menu1.setMenuDT(element.getMenuDT());
                    break;
                case 2:
                    menu2.addMenuElement(element);
                    menu2.setMenuDT(element.getMenuDT());
                    break;
                case 3:
                    menu3.addMenuElement(element);
                    menu3.setMenuDT(element.getMenuDT());
                    break;
                case 4:
                    menu4.addMenuElement(element);
                    menu4.setMenuDT(element.getMenuDT());
                    break;
                case 5:
                    menu5.addMenuElement(element);
                    menu5.setMenuDT(element.getMenuDT());
                    break;
                case 6:
                    menu6.addMenuElement(element);
                    menu6.setMenuDT(element.getMenuDT());
                    break;
            }
        }

        weekMenuByDay.put(1,menu1);
        weekMenuByDay.put(2,menu2);
        weekMenuByDay.put(3,menu3);
        weekMenuByDay.put(4,menu4);
        weekMenuByDay.put(5,menu5);
        weekMenuByDay.put(6,menu6);

        logger.info("Week menu by day was provided.");
        return weekMenuByDay;
    }

    public synchronized void buildWeekMenu()
    {

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);

        Date weekStart = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, 6);
        Date weekEnd = c.getTime();

        MenuDAO menuDAO = new MenuDAO();
        weekMenu = menuDAO.retrieveEntitiesInRange(dishes, weekStart, weekEnd);
        menuDAO.returnConnectionToPool();

        logger.info("Week menu is built. Details: " + weekMenu.toString());
    }

    public synchronized List<MenuElement> retrieveWholeMenu() throws BusinessLogicException
    {
        if(weekMenu==null)
        {
            logger.error("Attempt to retrieve empty menu.");
            throw new BusinessLogicException("Whole menu is empty.");
        }

        logger.info("Whole menu was provided.");
        return wholeMenu;
    }

    public synchronized void buildWholeMenu()
    {
        MenuDAO menuDAO = new MenuDAO();
        wholeMenu = menuDAO.retrieveAllEntities(dishes);
        menuDAO.returnConnectionToPool();

        logger.info("Whole menu is built. Details: " + wholeMenu.toString());
    }
}
