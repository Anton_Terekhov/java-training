package by.aterekhov.epamcoffee.shareapplicationobject;


import by.aterekhov.epamcoffee.data.OrderItemStatus;
import by.aterekhov.epamcoffee.data.OrderType;
import by.aterekhov.epamcoffee.data.UserRole;
import by.aterekhov.epamcoffee.database.daoelement.OrderItemStatusDAO;
import by.aterekhov.epamcoffee.database.daoelement.OrderTypeDAO;
import by.aterekhov.epamcoffee.database.daoelement.UserRoleDAO;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public enum ShareInternalObject
{
    APPLICATION;

    private List<UserRole> userRoles;
    private List<OrderType> orderTypes;
    private List<OrderItemStatus> orderItemStatuses;

    private static Logger logger = LogManager.getLogger(ShareInternalObject.class);

    ShareInternalObject()
    {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        userRoles = userRoleDAO.retrieveAllEntities();
        userRoleDAO.returnConnectionToPool();
        LogManager.getLogger(ShareInternalObject.class).info("User roles are built. Details: " + userRoles.toString());

        OrderTypeDAO orderTypeDAO = new OrderTypeDAO();
        orderTypes = orderTypeDAO.retrieveAllEntities();
        orderTypeDAO.returnConnectionToPool();
        LogManager.getLogger(ShareInternalObject.class).info("Order types are built. Details: " + orderTypes.toString());

        OrderItemStatusDAO orderItemStatusDAO = new OrderItemStatusDAO();
        orderItemStatuses = orderItemStatusDAO.retrieveAllEntities();
        orderItemStatusDAO.returnConnectionToPool();
        LogManager.getLogger(ShareInternalObject.class).info("Order Item statuses are built. Details: " + orderItemStatuses.toString());
    }

    public synchronized List<UserRole> retrieveUserRoles() throws BusinessLogicException
    {
        if(userRoles==null)
        {
            logger.error("Attempt to retrieve empty list of user roles.");
            throw new BusinessLogicException("User roles aren't defined (empty).");
        }

        logger.info("User roles were provided.");
        return userRoles;
    }

    public synchronized void buildDishTypes()
    {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        userRoles = userRoleDAO.retrieveAllEntities();
        userRoleDAO.returnConnectionToPool();
        logger.info("User roles are built. Details: " + userRoles.toString());
    }

    public synchronized List<OrderType> retrieveOrderTypes() throws BusinessLogicException
    {
        if(orderTypes==null)
        {
            logger.error("Attempt to retrieve empty list of order types.");
            throw new BusinessLogicException("Order types aren't defined (empty).");
        }

        logger.info("Order types were provided.");
        return orderTypes;
    }

    public synchronized void buildOrderTypes()
    {
        OrderTypeDAO orderTypeDAO = new OrderTypeDAO();
        orderTypes = orderTypeDAO.retrieveAllEntities();
        orderTypeDAO.returnConnectionToPool();
        logger.info("Order types are built. Details: " + orderTypes.toString());
    }

    public synchronized List<OrderItemStatus> retrieveOrderItemStatuses() throws BusinessLogicException
    {
        if(orderItemStatuses==null)
        {
            logger.error("Attempt to retrieve empty list of order item statuses.");
            throw new BusinessLogicException("Order item statuses aren't defined (empty).");
        }

        logger.info("Order item statuses were provided.");
        return orderItemStatuses;
    }

    public synchronized void buildOrderItemStatuses()
    {
        OrderItemStatusDAO orderItemStatusDAO = new OrderItemStatusDAO();
        orderItemStatuses = orderItemStatusDAO.retrieveAllEntities();
        orderItemStatusDAO.returnConnectionToPool();
        logger.info("Order Item statuses are built. Details: " + orderItemStatuses.toString());
    }
}
