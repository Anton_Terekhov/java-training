package by.aterekhov.epamcoffee.data;


public class DishType extends Entity
{

    private String dishTypeNameRus;
    private String dishTypeNameEng;
    private int dishTypeSortOrder;

    public DishType(){}

    public DishType(int dishTypeId, String dishTypeNameRus, String dishTypeNameEng, int dishTypeSortOrder)
    {
        super(dishTypeId);
        this.dishTypeNameRus = dishTypeNameRus;
        this.dishTypeNameEng = dishTypeNameEng;
        this.dishTypeSortOrder = dishTypeSortOrder;
    }

    public String getDishTypeNameRus()
    {
        return dishTypeNameRus;
    }

    public void setDishTypeNameRus(String dishTypeNameRus)
    {
        this.dishTypeNameRus = dishTypeNameRus;
    }

    public String getDishTypeNameEng() {
        return dishTypeNameEng;
    }

    public void setDishTypeNameEng(String dishTypeNameEng)
    {
        this.dishTypeNameEng = dishTypeNameEng;
    }

    public int getDishTypeSortOrder()
    {
        return dishTypeSortOrder;
    }

    public void setDishTypeSortOrder(int dishTypeSortOrder)
    {
        this.dishTypeSortOrder = dishTypeSortOrder;
    }

    @Override
    public String toString()
    {
        return "DishType: " +
                dishTypeNameEng +
                " [" + dishTypeSortOrder + "]";
    }
}
