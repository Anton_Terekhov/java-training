package by.aterekhov.epamcoffee.data;


import java.util.Calendar;
import java.util.Date;

public class MenuElement extends Entity
{
    private Dish dish;
    private Date menuDT = new Date();
    private double cost;

    public MenuElement(){}

    public MenuElement(int menuElementId, Dish dish, Date menuDT, double cost)
    {
        super(menuElementId);
        this.dish = dish;
        this.menuDT = menuDT;
        this.cost = cost;
    }

    public Dish getDish()
    {
        return dish;
    }

    public void setDish(Dish dish)
    {
        this.dish = dish;
    }

    public Date getMenuDT()
    {
        return menuDT;
    }

    public int getDayOfWeek()
    {
        Calendar c = Calendar.getInstance();
        c.setTime(menuDT);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    public void setMenuDT(Date menuDT)
    {
        this.menuDT = menuDT;
    }

    public double getCost()
    {
        return cost;
    }

    public void setCost(double cost)
    {
        this.cost = cost;
    }

    @Override
    public String toString()
    {
        return "Menu element on " +
                menuDT +
                " is " + dish.getDishNameEng() +
                " (" + cost + " BYN)";
    }
}
