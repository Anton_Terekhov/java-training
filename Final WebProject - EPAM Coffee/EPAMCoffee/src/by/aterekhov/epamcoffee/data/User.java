package by.aterekhov.epamcoffee.data;



public class User extends Entity
{
    private String userFirstNameRus;
    private String userFirstNameEng;
    private String userSecondNameRus;
    private String userSecondNameEng;
    private String userEmail;
    private String userPassword;
    private UserRole userRole;

    public User(){}

    public User(int id, String userFirstNameRus, String userFirstNameEng, String userSecondNameRus, String userSecondNameEng, String userEmail, String userPassword, UserRole userRole)
    {
        super(id);
        this.userFirstNameRus = userFirstNameRus;
        this.userFirstNameEng = userFirstNameEng;
        this.userSecondNameRus = userSecondNameRus;
        this.userSecondNameEng = userSecondNameEng;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userRole = userRole;
    }

    public String getUserFirstNameRus()
    {
        return userFirstNameRus;
    }

    public void setUserFirstNameRus(String userFirstNameRus)
    {
        this.userFirstNameRus = userFirstNameRus;
    }

    public String getUserFirstNameEng()
    {
        return userFirstNameEng;
    }

    public void setUserFirstNameEng(String userFirstNameEng)
    {
        this.userFirstNameEng = userFirstNameEng;
    }

    public String getUserSecondNameRus()
    {
        return userSecondNameRus;
    }

    public void setUserSecondNameRus(String userSecondNameRus)
    {
        this.userSecondNameRus = userSecondNameRus;
    }

    public String getUserSecondNameEng()
    {
        return userSecondNameEng;
    }

    public void setUserSecondNameEng(String userSecondNameEng)
    {
        this.userSecondNameEng = userSecondNameEng;
    }

    public String getUserEmail()
    {
        return userEmail;
    }

    public void setUserEmail(String userEmail)
    {
        this.userEmail = userEmail;
    }

    public String getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }

    public UserRole getUserRole()
    {
        return userRole;
    }

    public void setUserRole(UserRole userRole)
    {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "User " +
                userFirstNameEng + " " + userSecondNameEng +
                " (" + userEmail + ") [" +
                userRole.getRoleNameEng() + "]";
    }
}
