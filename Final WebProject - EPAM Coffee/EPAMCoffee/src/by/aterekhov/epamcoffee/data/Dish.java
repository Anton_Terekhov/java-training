package by.aterekhov.epamcoffee.data;

public class Dish extends Entity
{
    private String dishNameRus;
    private String dishNameEng;
    private DishType dishType;
    private double weight;
    private String dishCommentRus;
    private String dishCommentEng;

    public Dish() {}

    public Dish(int dishId, String dishNameRus, String dishNameEng, DishType dishType, double weight, String dishCommentRus, String dishCommentEng)
    {
        super(dishId);
        this.dishNameRus = dishNameRus;
        this.dishNameEng = dishNameEng;
        this.dishType = dishType;
        this.weight = weight;
        this.dishCommentRus = dishCommentRus;
        this.dishCommentEng = dishCommentEng;
    }

    public String getDishNameRus()
    {
        return dishNameRus;
    }

    public void setDishNameRus(String dishNameRus)
    {
        this.dishNameRus = dishNameRus;
    }

    public String getDishNameEng()
    {
        return dishNameEng;
    }

    public void setDishNameEng(String dishNameEng)
    {
        this.dishNameEng = dishNameEng;
    }

    public DishType getDishType()
    {
        return dishType;
    }

    public void setDishType(DishType dishType)
    {
        this.dishType = dishType;
    }

    public double getWeight()
    {
        return weight;
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    public String getDishCommentRus()
    {
        return dishCommentRus;
    }

    public void setDishCommentRus(String dishCommentRus)
    {
        this.dishCommentRus = dishCommentRus;
    }

    public String getDishCommentEng()
    {
        return dishCommentEng;
    }

    public void setDishCommentEng(String dishCommentEng)
    {
        this.dishCommentEng = dishCommentEng;
    }

    @Override
    public String toString()
    {
        return "Dish: " +
                dishNameEng +
                " [ " + dishType.getDishTypeNameEng() +
                "] " + weight + " g";
    }
}
