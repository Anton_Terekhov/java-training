package by.aterekhov.epamcoffee.data;


public class OrderItemStatus extends Entity
{
    private String orderItemStatusNameRus;
    private String orderItemStatusNameEng;
    private String orderItemStatusDescriptionRus;
    private String orderItemStatusDescriptionEng;

    public OrderItemStatus() {}

    public OrderItemStatus(int id, String orderItemStatusNameRus, String orderItemStatusNameEng, String orderItemStatusDescriptionRus, String orderItemStatusDescriptionEng)
    {
        super(id);
        this.orderItemStatusNameRus = orderItemStatusNameRus;
        this.orderItemStatusNameEng = orderItemStatusNameEng;
        this.orderItemStatusDescriptionRus = orderItemStatusDescriptionRus;
        this.orderItemStatusDescriptionEng = orderItemStatusDescriptionEng;
    }

    public String getOrderItemStatusNameRus()
    {
        return orderItemStatusNameRus;
    }

    public void setOrderItemStatusNameRus(String orderItemStatusNameRus)
    {
        this.orderItemStatusNameRus = orderItemStatusNameRus;
    }

    public String getOrderItemStatusNameEng()
    {
        return orderItemStatusNameEng;
    }

    public void setOrderItemStatusNameEng(String orderItemStatusNameEng)
    {
        this.orderItemStatusNameEng = orderItemStatusNameEng;
    }

    public String getOrderItemStatusDescriptionRus()
    {
        return orderItemStatusDescriptionRus;
    }

    public void setOrderItemStatusDescriptionRus(String orderItemStatusDescriptionRus)
    {
        this.orderItemStatusDescriptionRus = orderItemStatusDescriptionRus;
    }

    public String getOrderItemStatusDescriptionEng()
    {
        return orderItemStatusDescriptionEng;
    }

    public void setOrderItemStatusDescriptionEng(String orderItemStatusDescriptionEng)
    {
        this.orderItemStatusDescriptionEng = orderItemStatusDescriptionEng;
    }

    @Override
    public String toString()
    {
        return "OrderItemStatus: " +
                orderItemStatusNameEng;
    }
}
