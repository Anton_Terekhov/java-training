package by.aterekhov.epamcoffee.data;


public class OrderType extends Entity
{
    private String orderTypeNameRus;
    private String orderTypeNameEng;
    private String orderTypeDescriptionRus;
    private String orderTypeDescriptionEng;

    public OrderType () {}

    public OrderType(int id, String orderTypeNameRus, String orderTypeNameEng, String orderTypeDescriptionRus, String orderTypeDescriptionEng)
    {
        super(id);
        this.orderTypeNameRus = orderTypeNameRus;
        this.orderTypeNameEng = orderTypeNameEng;
        this.orderTypeDescriptionRus = orderTypeDescriptionRus;
        this.orderTypeDescriptionEng = orderTypeDescriptionEng;
    }

    public String getOrderTypeNameRus()
    {
        return orderTypeNameRus;
    }

    public void setOrderTypeNameRus(String orderTypeNameRus)
    {
        this.orderTypeNameRus = orderTypeNameRus;
    }

    public String getOrderTypeNameEng()
    {
        return orderTypeNameEng;
    }

    public void setOrderTypeNameEng(String orderTypeNameEng)
    {
        this.orderTypeNameEng = orderTypeNameEng;
    }

    public String getOrderTypeDescriptionRus()
    {
        return orderTypeDescriptionRus;
    }

    public void setOrderTypeDescriptionRus(String orderTypeDescriptionRus)
    {
        this.orderTypeDescriptionRus = orderTypeDescriptionRus;
    }

    public String getOrderTypeDescriptionEng()
    {
        return orderTypeDescriptionEng;
    }

    public void setOrderTypeDescriptionEng(String orderTypeDescriptionEng)
    {
        this.orderTypeDescriptionEng = orderTypeDescriptionEng;
    }

    @Override
    public String toString()
    {
        return "OrderType: " +
                orderTypeNameEng;
    }
}
