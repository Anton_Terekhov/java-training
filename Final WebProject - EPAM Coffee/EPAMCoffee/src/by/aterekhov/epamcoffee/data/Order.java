package by.aterekhov.epamcoffee.data;

import java.util.Date;

public class Order extends Entity
{
    private Date orderDT;
    private User user;
    private OrderType orderType;
    private String orderComment = "";

    public Order(){}

    public Order(int id, Date orderDT, User user, OrderType orderType, String orderComment)
    {
        super(id);
        this.orderDT = orderDT;
        this.user = user;
        this.orderType = orderType;
        this.orderComment = orderComment;
    }

    public Date getOrderDT()
    {
        return orderDT;
    }

    public void setOrderDT(Date orderDT)
    {
        this.orderDT = orderDT;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public OrderType getOrderType()
    {
        return orderType;
    }

    public void setOrderType(OrderType orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderComment()
    {
        return orderComment;
    }

    public void setOrderComment(String orderComment)
    {
        this.orderComment = orderComment;
    }

    @Override
    public String toString() {
        return "Order #" + getId() + " on " +
                orderDT +
                " of " + user.getUserEmail() +
                " (" + orderType.getOrderTypeNameEng() +
                ") [" + orderComment + "]";
    }
}
