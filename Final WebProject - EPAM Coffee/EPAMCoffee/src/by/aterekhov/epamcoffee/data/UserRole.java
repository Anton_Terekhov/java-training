package by.aterekhov.epamcoffee.data;


public class UserRole extends Entity
{
    private String roleNameRus;
    private String roleNameEng;
    private String roleDescriptionRus;
    private String roleDescriptionEng;

    public UserRole() {}

    public UserRole(int id, String RoleNameRus, String RoleNameEng, String RoleDescriptionRus, String RoleDescriptionEng)
    {
        super(id);
        this.roleNameRus = RoleNameRus;
        this.roleNameEng = RoleNameEng;
        this.roleDescriptionRus = RoleDescriptionRus;
        this.roleDescriptionEng = RoleDescriptionEng;
    }

    public String getRoleNameRus()
    {
        return roleNameRus;
    }

    public void setRoleNameRus(String RoleNameRus)
    {
        this.roleNameRus = RoleNameRus;
    }

    public String getRoleNameEng()
    {
        return roleNameEng;
    }

    public void setRoleNameEng(String RoleNameEng)
    {
        this.roleNameEng = RoleNameEng;
    }

    public String getRoleDescriptionRus()
    {
        return roleDescriptionRus;
    }

    public void setRoleDescriptionRus(String RoleDescriptionRus)
    {
        this.roleDescriptionRus = RoleDescriptionRus;
    }

    public String getRoleDescriptionEng()
    {
        return roleDescriptionEng;
    }

    public void setRoleDescriptionEng(String RoleDescriptionEng)
    {
        this.roleDescriptionEng = RoleDescriptionEng;
    }

    @Override
    public String toString()
    {
        return "User Role: " +
                roleNameEng;
    }
}
