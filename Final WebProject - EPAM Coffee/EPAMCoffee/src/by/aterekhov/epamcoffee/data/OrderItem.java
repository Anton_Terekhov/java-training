package by.aterekhov.epamcoffee.data;

public class OrderItem extends Entity
{
    private MenuElement menuElement;
    private Order order;
    private OrderItemStatus orderItemStatus;
    private double Amount;

    public OrderItem() {}

    public OrderItem(int id, MenuElement menuElement, Order order, OrderItemStatus orderItemStatus, double amount)
    {
        super(id);
        this.menuElement = menuElement;
        this.order = order;
        this.orderItemStatus = orderItemStatus;
        Amount = amount;
    }

    public MenuElement getMenuElement()
    {
        return menuElement;
    }

    public void setMenuElement(MenuElement menuElement)
    {
        this.menuElement = menuElement;
    }

    public Order getOrder()
    {
        return order;
    }

    public void setOrder(Order order)
    {
        this.order = order;
    }

    public OrderItemStatus getOrderItemStatus()
    {
        return orderItemStatus;
    }

    public void setOrderItemStatus(OrderItemStatus orderItemStatus)
    {
        this.orderItemStatus = orderItemStatus;
    }

    public double getAmount()
    {
        return Amount;
    }

    public void setAmount(double amount)
    {
        Amount = amount;
    }

    @Override
    public String toString() {
        return "OrderItem #" + super.getId() +
                ": " + menuElement.toString() +
                " [" + orderItemStatus.getOrderItemStatusNameEng() +
                "] x " + Amount;
    }
}
