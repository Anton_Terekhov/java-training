package by.aterekhov.epamcoffee.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DayMenu
{
    private Date menuDT = new Date();
    private ArrayList<MenuElement> menuElements = new ArrayList<>();

    public DayMenu()
    { }

    public Date getMenuDT()
    {
        return menuDT;
    }

    public void setMenuDT(Date menuDT)
    {
        this.menuDT = menuDT;
    }

    public int getDayOfWeek()
    {
        Calendar c = Calendar.getInstance();
        c.setTime(menuDT);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    public ArrayList<MenuElement> getMenuElements()
    {
        return menuElements;
    }

    public void setMenuElements(ArrayList<MenuElement> menuElements)
    {
        this.menuElements = menuElements;
    }

    public void addMenuElement(MenuElement element)
    {
        menuElements.add(element);
    }
}
