package by.aterekhov.epamcoffee.exception;

public class BusinessLogicException extends Exception
{
    public BusinessLogicException()
    {
    }

    public BusinessLogicException(String message, Throwable exception)
    {
        super(message, exception);
    }

    public BusinessLogicException(String message)
    {
        super(message);
    }

    public BusinessLogicException (Throwable exception)
    {
        super(exception);
    }
}
