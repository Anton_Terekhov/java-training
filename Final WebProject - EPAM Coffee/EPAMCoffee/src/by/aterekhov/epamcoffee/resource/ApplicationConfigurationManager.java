package by.aterekhov.epamcoffee.resource;
import java.util.ResourceBundle;

public class ApplicationConfigurationManager
{
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private ApplicationConfigurationManager() { }

    public static String getProperty(String key)
    {
        return resourceBundle.getString(key);
    }
}
