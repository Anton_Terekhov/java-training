package by.aterekhov.epamcoffee.command.client;

import by.aterekhov.epamcoffee.command.*;

public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOCALE {
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    MENU {
        {
            this.command = new MainMenuCommand();
        }
    },
    ORDER {
        {
            this.command = new MakeOrderCommand();
        }
    },
    HISTORY {
        {
            this.command = new OrderHistoryCommand();
        }
    },
    PROFILE {
        {
            this.command = new ProfileInfoCommand();
        }
    },
    ABOUT {
        {
            this.command = new AboutCommand();
        }
    },
    ADMINISTRATIVE {
        {
            this.command = new ShowAdminInfoCommand();
        }
    },
    DELETE_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },
    DECLINE_ORDER_ITEM {
        {
            this.command = new DeclineOrderItemCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand()
    {
        return command;
    }
}