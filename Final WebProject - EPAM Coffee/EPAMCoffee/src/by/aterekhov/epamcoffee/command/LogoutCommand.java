package by.aterekhov.epamcoffee.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogoutCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = ApplicationConfigurationManager.getProperty("path.page.index");
        request.getSession().invalidate();
        logger.info("User is going to index page.");
        return page;
    }
}