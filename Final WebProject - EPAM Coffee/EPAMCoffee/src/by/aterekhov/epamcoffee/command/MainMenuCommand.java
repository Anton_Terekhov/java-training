package by.aterekhov.epamcoffee.command;


import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.shareapplicationobject.ShareMenu;
import by.aterekhov.epamcoffee.data.DayMenu;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class MainMenuCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(MainMenuCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        logger.info("Retrieving of menu for current week.");

        try
        {
            Map weekMenuByDay = ShareMenu.MENU.retrieveWeekMenuByDay();

            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);

            DayMenu todayMenu;
            todayMenu = (DayMenu) weekMenuByDay.get(day);

            DayMenu menu1 = (DayMenu) weekMenuByDay.get(1);
            DayMenu menu2 = (DayMenu) weekMenuByDay.get(2);
            DayMenu menu3 = (DayMenu) weekMenuByDay.get(3);
            DayMenu menu4 = (DayMenu) weekMenuByDay.get(4);
            DayMenu menu5 = (DayMenu) weekMenuByDay.get(5);
            DayMenu menu6 = (DayMenu) weekMenuByDay.get(6);

            request.setAttribute("todaymenu", todayMenu);
            //request.setAttribute("todaymenu", menu3);
            request.setAttribute("menu1", menu1);
            request.setAttribute("menu2", menu2);
            request.setAttribute("menu3", menu3);
            request.setAttribute("menu4", menu4);
            request.setAttribute("menu5", menu5);
            request.setAttribute("menu6", menu6);
            request.getRequestDispatcher("jstl_foreach.jsp");
            logger.info("Week menu by day were passed to user.");

            logger.info("User is going to Main menu page.");
            page = ApplicationConfigurationManager.getProperty("path.page.main");
        }
        catch (BusinessLogicException e)
        {
            logger.error("Building of main menu was failed. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }

}
