package by.aterekhov.epamcoffee.command;


import by.aterekhov.epamcoffee.applicationlogic.OrderLogic;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeclineOrderItemCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(DeclineOrderItemCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try
        {
            int itemId = Integer.parseInt(request.getParameter("itemid"));
            OrderLogic.declineOrderItemById(itemId);
            logger.info("Declining of order item was successful.");
            ActionCommand command = new OrderHistoryCommand();
            page = command.execute(request, response);
        }
        catch (BusinessLogicException e)
        {
            logger.error("Decline operation was failed. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
