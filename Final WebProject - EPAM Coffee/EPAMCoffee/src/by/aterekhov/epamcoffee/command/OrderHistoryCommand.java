package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.applicationlogic.OrderLogic;
import by.aterekhov.epamcoffee.data.Order;
import by.aterekhov.epamcoffee.data.OrderItem;
import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class OrderHistoryCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(OrderHistoryCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try
        {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");

            List<Order> orders = OrderLogic.retrieveAllOrders(user);
            if(orders != null)
            {
                logger.info("Orders of user were retrieved.");
                List<OrderItem> orderItems = OrderLogic.retrieveAllOrderItems(orders);
                if(orderItems != null)
                {
                    logger.info("Order items were retrieved.");
                    request.setAttribute("orders", orders);
                    request.setAttribute("orderitems", orderItems);
                }
                else
                {
                    logger.info("User orders doesn't have any order items.");
                }
            }
            else
            {
                logger.info("User doesn't have orders.");
            }

            logger.info("User is going to Orders page.");
            page = ApplicationConfigurationManager.getProperty("path.page.orders");

        }
        catch (BusinessLogicException e)
        {
            logger.error("Retrieving of user's history was failed. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
