package by.aterekhov.epamcoffee.command.factory;

import javax.servlet.http.HttpServletRequest;
import by.aterekhov.epamcoffee.command.ActionCommand;
import by.aterekhov.epamcoffee.command.EmptyCommand;
import by.aterekhov.epamcoffee.command.client.CommandEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ActionFactory
{
    private static Logger logger = LogManager.getLogger(ActionFactory.class);

    public ActionCommand defineCommand(HttpServletRequest request)
    {
        ActionCommand current = new EmptyCommand();

        String action = request.getParameter("command");
        if (action == null || action.isEmpty())
        {
            return current;
        }

        try
        {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            logger.info("User sent command '" + action.toUpperCase() + "'.");
            current = currentEnum.getCurrentCommand();
        }
        catch (IllegalArgumentException e)
        {
            logger.info("Was sent NOT supported command '" + action.toUpperCase() + "'.");
        }

        return current;
    }
}