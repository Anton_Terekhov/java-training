package by.aterekhov.epamcoffee.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.aterekhov.epamcoffee.applicationlogic.UserLogic;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginCommand implements ActionCommand
{
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    private static Logger logger = LogManager.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try
        {
            String login = request.getParameter(PARAM_NAME_LOGIN);
            String pass = request.getParameter(PARAM_NAME_PASSWORD);
            logger.info("User entered credentials: " + login + "/" + pass + ".");

            if (UserLogic.checkLogin(login, pass)) {
                logger.info("User passed authentication.");
                HttpSession session = request.getSession();
                session.setAttribute("login", login);
                session.setAttribute("user", UserLogic.getUser(login));
                session.setAttribute("group", UserLogic.getUser(login).getUserRole().getRoleNameEng());

                ActionCommand command = new MainMenuCommand();
                page = command.execute(request, response);
            } else {
                logger.info("User failed authentication.");
                request.setAttribute("errorLoginPass", true);
                logger.info("User is going to Login page.");
                page = ApplicationConfigurationManager.getProperty("path.page.login");
            }
        }
        catch (BusinessLogicException e)
        {
            logger.error("Internal error during user roles initialization. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}