package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.applicationlogic.UserLogic;
import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowAdminInfoCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(ShowAdminInfoCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try
        {
            List<User> users = UserLogic.getAllUsers();

            if (users != null) {
                logger.info("Users were retrieved.");
                request.setAttribute("users", users);
            } else {
                logger.info("Users weren't retrieved. Application doesn't have users.");
            }

            logger.info("User is going to Admin page.");
            page = ApplicationConfigurationManager.getProperty("path.page.administrative");
        }
        catch (BusinessLogicException e)
        {
            logger.error("Internal error during user roles initialization. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
