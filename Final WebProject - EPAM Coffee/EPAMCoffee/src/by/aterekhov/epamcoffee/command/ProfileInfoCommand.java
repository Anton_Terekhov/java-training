package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ProfileInfoCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(ProfileInfoCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page;

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        if(user != null)
        {
            logger.info("User information was retrieved.");
            request.setAttribute("user", user);

            logger.info("User is going to Profile page.");
            page = ApplicationConfigurationManager.getProperty("path.page.profile");
        }
        else
        {
            logger.info("User information can not be retrieved.");
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
