package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.applicationlogic.UserLogic;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteUserCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(DeleteUserCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try
        {
            int userId = Integer.parseInt(request.getParameter("userid"));
            UserLogic.deleteUserById(userId);
            logger.info("User was deleted");
            ActionCommand command = new ShowAdminInfoCommand();
            page = command.execute(request, response);
        }
        catch (BusinessLogicException e)
        {
            logger.error("Removing of user was failed. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
