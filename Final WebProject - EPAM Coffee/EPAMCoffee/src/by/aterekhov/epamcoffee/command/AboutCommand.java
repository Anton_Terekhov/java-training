package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AboutCommand implements ActionCommand
{
    private static Logger logger = LogManager.getLogger(AboutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = ApplicationConfigurationManager.getProperty("path.page.about");
        logger.info("User is going to About page.");
        return page;
    }
}
