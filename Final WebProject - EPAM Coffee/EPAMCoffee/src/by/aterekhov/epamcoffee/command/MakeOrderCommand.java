package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.applicationlogic.OrderLogic;
import by.aterekhov.epamcoffee.data.User;
import by.aterekhov.epamcoffee.exception.BusinessLogicException;
import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MakeOrderCommand implements ActionCommand
{
    private static final String PARAM_MENU_ELEMENT_ID = "menuelementid";
    private static final String PARAM_DISH_AMOUNT = "dishamount";

    private static final int ORDER_TYPE_ID = 1;
    private static final String ORDER_COMMENT = "";

    private static Logger logger = LogManager.getLogger(MakeOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page = null;

        try {
            String[] dishAmounts = request.getParameterValues(PARAM_DISH_AMOUNT);
            String[] menuElementIDs = request.getParameterValues(PARAM_MENU_ELEMENT_ID);

            boolean orderCreation = false;
            int orderId = 0;

            for (int i = 0; i < menuElementIDs.length; i++) {
                if (Integer.parseInt(dishAmounts[i]) > 0) {
                    if (!orderCreation) {
                        HttpSession session = request.getSession();
                        User user = (User) session.getAttribute("user");
                        orderId = OrderLogic.createOrder(user.getId(), ORDER_TYPE_ID, ORDER_COMMENT);
                        logger.info("New order was created with id = " + orderId + ".");
                        orderCreation = true;
                    }
                    OrderLogic.createOrderItems(orderId, Integer.parseInt(menuElementIDs[i]), Double.parseDouble(dishAmounts[i]));
                }
            }

            logger.info("All necessary order items were added to new order.");

            ActionCommand command = new OrderHistoryCommand();
            page = command.execute(request, response);

        }
        catch (BusinessLogicException e)
        {
            logger.error("Creation of order was failed. Details: " + e.toString());
        }

        if(page == null)
        {
            logger.info("User is going to Error page.");
            page = ApplicationConfigurationManager.getProperty("path.page.error");
        }

        return page;
    }
}
