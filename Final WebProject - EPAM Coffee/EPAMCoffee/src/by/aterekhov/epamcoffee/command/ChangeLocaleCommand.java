package by.aterekhov.epamcoffee.command;

import by.aterekhov.epamcoffee.resource.ApplicationConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeLocaleCommand implements ActionCommand
{
    private static final String PARAM_LANGUAGE = "language";
    private static final String PARAM_CURRENT_PAGE = "page";

    private static Logger logger = LogManager.getLogger(ChangeLocaleCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
    {
        String page;

        String locale = request.getParameter(PARAM_LANGUAGE);

        HttpSession session = request.getSession();
        String currentPage = (String) session.getAttribute(PARAM_CURRENT_PAGE);

        if(locale.equals("ru_RU")&&!locale.equals(session.getAttribute("locale")))
        {
            session.setAttribute("locale", locale);
            logger.info("Locale is changed from en_US to ru_RU");
        }
        else if (locale.equals("en_US")&&!locale.equals(session.getAttribute("locale")))
        {
            session.setAttribute("locale", locale);
            logger.info("Locale is changed from ru_RU to en_US");
        }
        else
        {
            logger.info("Locale isn't needed to be changed and it's " + locale);
        }

        switch (currentPage)
        {
            case "login":
                page = ApplicationConfigurationManager.getProperty("path.page.login");
                break;
            case "main":
                ActionCommand commandMain = new MainMenuCommand();
                page = commandMain.execute(request, response);
                break;
            case "orders":
                ActionCommand commandOrders = new OrderHistoryCommand();
                page = commandOrders.execute(request, response);
                break;
            case "profile":
                ActionCommand commandProfile = new ProfileInfoCommand();
                page = commandProfile.execute(request, response);
                break;
            case "administrative":
                ActionCommand commandAdmin = new ShowAdminInfoCommand();
                page = commandAdmin.execute(request, response);
                break;
            case "about":
                page = ApplicationConfigurationManager.getProperty("path.page.about");
                break;
            default:
                page = ApplicationConfigurationManager.getProperty("path.page.index");
                break;
        }

        return page;
    }
}
