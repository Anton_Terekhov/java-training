USE epamcoffee;

DROP TABLE IF EXISTS OrderItemStatus;
CREATE TABLE OrderItemStatus
(
	OrderItemStatusId INT(6) AUTO_INCREMENT PRIMARY KEY,
    OrderItemStatusNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    OrderItemStatusNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    OrderItemStatusDescriptionRus VARCHAR(500) CHARACTER SET utf8 NOT NULL,
    OrderItemStatusDescriptionEng VARCHAR(500) CHARACTER SET utf8 NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL
);

INSERT INTO OrderItemStatus (OrderItemStatusNameRus, OrderItemStatusNameEng, OrderItemStatusDescriptionRus, OrderItemStatusDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('В процессе', 'In Progress', 'Незвершенный заказ (в процессе)', 'Not finished order (in progress)', NOW(), 'Admin', NULL, NULL);
INSERT INTO OrderItemStatus (OrderItemStatusNameRus, OrderItemStatusNameEng, OrderItemStatusDescriptionRus, OrderItemStatusDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Завершен', 'Done', 'Заказ выполнен (завершен)', 'Order was done (finished)', NOW(), 'Admin', NULL, NULL);
INSERT INTO OrderItemStatus (OrderItemStatusNameRus, OrderItemStatusNameEng, OrderItemStatusDescriptionRus, OrderItemStatusDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Провален', 'Failed', 'Заказ не выполнен (провален)', 'Order was failed ( NOT finished)', NOW(), 'Admin', NULL, NULL);
INSERT INTO OrderItemStatus (OrderItemStatusNameRus, OrderItemStatusNameEng, OrderItemStatusDescriptionRus, OrderItemStatusDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Отменен', 'Declined', 'Заказ отменени', 'Order was declined', NOW(), 'Admin', NULL, NULL);