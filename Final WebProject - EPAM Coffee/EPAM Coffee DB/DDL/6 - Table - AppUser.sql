USE epamcoffee;

DROP TABLE IF EXISTS AppUser;
CREATE TABLE AppUser
(
	UserId INT(6) AUTO_INCREMENT PRIMARY KEY,
    UserFirstNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    UserFirstNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    UserSecondNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    UserSecondNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    UserEmail VARCHAR(150) CHARACTER SET utf8 NOT NULL,
    UserPassword VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    UserRoleId INT(6) NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL,
    CONSTRAINT FK_AppUser_AppUserRole FOREIGN KEY (UserRoleId)
    REFERENCES AppUserRole(UserRoleId)
);

INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Антон', 'Anton', 'Терехов', 'Terekhov', 'Anton_Terekhov@epam.com', '1234', 1, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Иван', 'Ivan', 'Иванов', 'Ivanov', 'Ivan_Ivanov@epam.com', '1234', 2, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Петр', 'Petr', 'Иванов', 'Ivanov', 'Petr_Ivanov@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Ольга', 'Olga', 'Иванова', 'Ivanov', 'Olga_Ivanova@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Ирина', 'Irina', 'Иванова', 'Ivanov', 'Irina_Ivanova@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Руслан', 'Ruslan', 'Иванов', 'Ivanov', 'Ruslan_Ivanov@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Николай', 'Nikolai', 'Иванов', 'Ivanov', 'Nikolai_Ivanov@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO AppUser (UserFirstNameRus, UserFirstNameEng, UserSecondNameRus, UserSecondNameEng, UserEmail, UserPassword, UserRoleId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Демо', 'Demo', 'ДляУдаления', 'ToDelete', 'demo@epam.com', '1234', 3, NOW(), 'Admin', NULL, NULL);