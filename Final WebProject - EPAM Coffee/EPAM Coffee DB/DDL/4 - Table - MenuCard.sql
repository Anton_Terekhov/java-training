USE epamcoffee;

DROP TABLE IF EXISTS MenuCard;
CREATE TABLE MenuCard
(
	MenuCardId INT(6) AUTO_INCREMENT PRIMARY KEY,
    MenuDT DATE NOT NULL,
    DishId INT(6) NOT NULL,
    Cost DECIMAL(10,2) NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL,
    CONSTRAINT FK_MenuCard_Dish FOREIGN KEY (DishId)
    REFERENCES Dish(DishId)
);

/*2017-05-01 - 2017-05-05*/

/*2017-05-01*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 1, 1.55, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 4, 1.2, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 8, 1.65, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 10, 0.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 13, 1.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 18, 2.5, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-05-02*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-03*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 3, 0.98, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 5, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 12, 1.82, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 17, 4.3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 18, 3.12, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 19, 3.2, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 20, 3.21, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-03' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-04*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 1, 1.34, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 3, 0.76, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 7, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 8, 1.86, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 13, 1.52, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 14, 0.63, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 15, 0.7, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 16, 3.47, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 17, 3.84, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 18, 2.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 21, 3.07, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 22, 3.3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-04' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-05*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 2, 1.48, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 6, 1.52, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 8, 1.92, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 10, 1.39, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 13, 1.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 14, 0.52, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 16, 3.44, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 17, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 19, 4.1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 20, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 22, 3.3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-05' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    
/*2017-05-08 - 2017-05-12*/
    
/*2017-05-08*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 1, 1.51, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 4, 1.22, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 8, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 10, 0.62, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 13, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 15, 0.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 16, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 18, 2.58, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 19, 2.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 23, 1.67, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-08' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-05-09*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-09' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-10*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 3, 0.96, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 5, 1.47, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 7, 1.08, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 9, 0.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 12, 1.8, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 11, 1.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 14, 0.82, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 17, 4.14, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 18, 3.72, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 19, 3.13, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 20, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-10' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-11*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 1, 1.38, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 3, 0.88, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 7, 1.13, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 8, 1.72, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 13, 1.45, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 14, 0.7, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 15, 0.68, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 16, 3.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 17, 4.02, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 18, 3.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 21, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 22, 3.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-11' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-12*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 2, 1.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 6, 1.58, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 8, 1.83, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 10, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 13, 1.68, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 14, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 16, 3.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 17, 3.71, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 19, 3.85, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 20, 2.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 22, 3.37, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-12' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    
    
/*2017-05-15 - 2017-05-19*/
    
/*2017-05-15*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 1, 1.51, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 4, 1.22, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 8, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 10, 0.62, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 13, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 15, 0.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 16, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 18, 2.58, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 19, 2.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 23, 1.67, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-15' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-05-16*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-16' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-17*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 3, 0.96, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 5, 1.47, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 7, 1.08, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 9, 0.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 12, 1.8, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 11, 1.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 14, 0.82, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 17, 4.14, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 18, 3.72, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 19, 3.13, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 20, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-17' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-18*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 1, 1.38, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 3, 0.88, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 7, 1.13, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 8, 1.72, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 13, 1.45, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 14, 0.7, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 15, 0.68, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 16, 3.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 17, 4.02, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 18, 3.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 21, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 22, 3.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-18' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-19*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 2, 1.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 6, 1.58, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 8, 1.83, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 10, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 13, 1.68, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 14, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 16, 3.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 17, 3.71, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 19, 3.85, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 20, 2.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 22, 3.37, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-19' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    
    
/*2017-05-22 - 2017-05-26*/
    
/*2017-05-22*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 1, 1.51, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 4, 1.22, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 8, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 10, 0.62, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 13, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 15, 0.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 16, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 18, 2.58, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 19, 2.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 23, 1.67, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-22' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-05-23*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-23' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-24*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 3, 0.96, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 5, 1.47, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 7, 1.08, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 9, 0.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 12, 1.8, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 11, 1.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 14, 0.82, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 17, 4.14, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 18, 3.72, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 19, 3.13, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 20, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-24' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-25*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 1, 1.38, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 3, 0.88, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 7, 1.13, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 8, 1.72, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 13, 1.45, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 14, 0.7, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 15, 0.68, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 16, 3.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 17, 4.02, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 18, 3.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 21, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 22, 3.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-25' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-26*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 2, 1.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 6, 1.58, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 8, 1.83, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 10, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 13, 1.68, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 14, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 16, 3.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 17, 3.71, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 19, 3.85, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 20, 2.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 22, 3.37, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-26' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);



/*2017-05-29 - 2017-06-02*/
    
/*2017-05-29*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 1, 1.51, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 4, 1.22, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 8, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 10, 0.62, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 13, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 15, 0.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 16, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 18, 2.58, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 19, 2.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 23, 1.67, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-29' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-05-30*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-30' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-05-31*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 3, 0.96, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 5, 1.47, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 7, 1.08, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 9, 0.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 12, 1.8, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 11, 1.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 14, 0.82, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 17, 4.14, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 18, 3.72, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 19, 3.13, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 20, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-31' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-06-01*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 1, 1.38, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 3, 0.88, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 7, 1.13, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 8, 1.72, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 13, 1.45, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 14, 0.7, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 15, 0.68, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 16, 3.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 17, 4.02, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 18, 3.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 21, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 22, 3.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-01' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-06-02*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 2, 1.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 6, 1.58, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 8, 1.83, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 10, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 13, 1.68, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 14, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 16, 3.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 17, 3.71, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 19, 3.85, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 20, 2.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 22, 3.37, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-02' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    


/*2017-06-05 - 2017-06-09*/
    
/*2017-06-05*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 1, 1.51, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 4, 1.22, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 7, 1.17, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 8, 1.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 10, 0.62, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 13, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 15, 0.73, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 16, 3.73, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 18, 2.58, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 19, 2.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 20, 2.95, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 23, 1.67, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-05' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);
    

/*2017-06-06*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 2, 1.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 6, 1.3, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 9, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 11, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 14, 0.76, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 15, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 16, 3.85, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 17, 4.11, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 19, 2.98, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 22, 2.75, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 23, 1.6, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-06' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-06-07*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 3, 0.96, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 5, 1.47, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 7, 1.08, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 9, 0.9, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 12, 1.8, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 11, 1.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 14, 0.82, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 17, 4.14, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 18, 3.72, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 19, 3.13, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 20, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 21, 3.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-07' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-06-08*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 1, 1.38, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 3, 0.88, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 7, 1.13, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 8, 1.72, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 13, 1.45, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 14, 0.7, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 15, 0.68, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 16, 3.35, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 17, 4.02, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 18, 3.6, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 21, 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 22, 3.25, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-08' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);

/*2017-06-09*/
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 2, 1.4, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 6, 1.58, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 7, 1.1, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 8, 1.83, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 10, 1.24, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 13, 1.68, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 14, 0.55, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 16, 3.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 17, 3.71, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 19, 3.85, NOW(), 'Admin', NULL, NULL);

INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 20, 2.2, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 22, 3.37, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 24, 1.46, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 25, 0.93, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 26, 0.89, NOW(), 'Admin', NULL, NULL);
INSERT INTO MenuCard (MenuDT, DishId, Cost, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-06-09' AS DATE), 27, 0.76, NOW(), 'Admin', NULL, NULL);