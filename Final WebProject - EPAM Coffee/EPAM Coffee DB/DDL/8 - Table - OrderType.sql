USE epamcoffee;

DROP TABLE IF EXISTS OrderType;
CREATE TABLE OrderType
(
	OrderTypeId INT(6) AUTO_INCREMENT PRIMARY KEY,
    OrderTypeNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    OrderTypeNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    OrderTypeDescriptionRus VARCHAR(500) CHARACTER SET utf8 NOT NULL,
    OrderTypeDescriptionEng VARCHAR(500) CHARACTER SET utf8 NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL
);

INSERT INTO OrderType (OrderTypeNameRus, OrderTypeNameEng, OrderTypeDescriptionRus, OrderTypeDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Заказ', 'Order', 'Сегодняшний заказ', 'Today''s order', NOW(), 'Admin', NULL, NULL);
INSERT INTO OrderType (OrderTypeNameRus, OrderTypeNameEng, OrderTypeDescriptionRus, OrderTypeDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Предзаказ', 'Pre-order', 'Предзаказ на будущее (другие дни текущей недели)', 'Pre-order for future (another day of current week)', NOW(), 'Admin', NULL, NULL);
INSERT INTO OrderType (OrderTypeNameRus, OrderTypeNameEng, OrderTypeDescriptionRus, OrderTypeDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Набросок', 'Draft order', 'Незаконченный (черновой) заказ', 'Draft order (without submit)', NOW(), 'Admin', NULL, NULL);