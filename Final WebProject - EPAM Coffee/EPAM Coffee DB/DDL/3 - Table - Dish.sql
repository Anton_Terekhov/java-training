USE epamcoffee;

DROP TABLE IF EXISTS Dish;
CREATE TABLE Dish
(
	DishId INT(6) AUTO_INCREMENT PRIMARY KEY,
	DishNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
	DishNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
	DishTypeId INT(6) NOT NULL,
    Weight DECIMAL(20,5) NULL,
    DishCommentRus VARCHAR(500) CHARACTER SET utf8 NULL,
	DishCommentEng VARCHAR(500) CHARACTER SET utf8 NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL,
    CONSTRAINT FK_Dish_DishType FOREIGN KEY (DishTypeId)
    REFERENCES DishType(DishTypeId)
);


INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Харчо', 'Kharcho', 1, 250, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Борщ', 'Borsch', 1, 270, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Куриный бульон', 'Chicken broth', 1, 220, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Зеленые Щи', 'Sorrel soup', 1, 250, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Перловый суп с грибами', 'Pearl-barley soup with mushrooms', 1, 280, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Рассольник', 'Pickle soup', 1, 250, NULL, NULL, NOW(), 'Admin', NULL, NULL);
    
    
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Греческий салат', 'Greek salad', 2, 140, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Оливье', 'Russian salad', 2, 120, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Овощная нарезка', 'Vegetable cuts', 2, 180, 'Огурцы, помидоры, красный перец', 'Cucumbers, tomatos, red pepper', NOW(), 'Admin', NULL, NULL);
    
    
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Рис', 'Rice', 3, 135, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Тушенные овощи', 'Smother vegetable', 3, 150, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Картофель фри', 'Chips', 3, 120, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Пюре', 'Mash', 3, 120, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Перловка', 'Barley groats', 3, 110, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Гречка', 'Buckwheat', 3, 115, NULL, NULL, NOW(), 'Admin', NULL, NULL);
    
    
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Свинина', 'Pork', 4, 105, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Говядина', 'Beef', 4, 90, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Курица', 'Сhicken', 4, 120, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Рыба', 'Fish', 4, 110, NULL, NULL, NOW(), 'Admin', NULL, NULL);
    
    
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Торт "Графские развалины"', 'Cake "Palatine ruins"', 5, 120, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Торт "Наполеон"', 'Cake "Napoleon"', 5, 150, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Торт "Трюфель"', 'Cake "Truffle"', 5, 100, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Пирог с яблоками', 'Pasty', 5, 100, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Шоколад Nestle', 'Сhocolate Nestle', 5, 90, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Батончик Snikers', 'Sweet bar Snikers', 5, 50, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Батончик Mars', 'Sweet bar Mars', 5, 50, NULL, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO Dish (DishNameRus, DishNameEng, DishTypeId, Weight, DishCommentRus, DishCommentEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Батончик KitKat', 'Sweet bar KitKat', 5, 50, NULL, NULL, NOW(), 'Admin', NULL, NULL);