USE epamcoffee;

DROP TABLE IF EXISTS UserOrder;
CREATE TABLE UserOrder
(
	OrderId INT(6) AUTO_INCREMENT PRIMARY KEY,
    OrderDT DATE NOT NULL,
    UserId INT(6) NOT NULL,
    OrderTypeId INT(6) NOT NULL,
    OrderComment VARCHAR(2000) CHARACTER SET utf8 NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL,
	CONSTRAINT FK_UserOrder_AppUser FOREIGN KEY (UserId)
    REFERENCES AppUser(UserId),
    CONSTRAINT FK_UserOrder_OrderType FOREIGN KEY (OrderTypeId)
    REFERENCES OrderType(OrderTypeId)
);

INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 3, 1, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-01' AS DATE), 5, 1, NULL, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 3, 1, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 4, 1, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 5, 1, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 6, 1, NULL, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrder (OrderDT, UserId, OrderTypeId, OrderComment, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (CAST('2017-05-02' AS DATE), 6, 1, NULL, NOW(), 'Admin', NULL, NULL);