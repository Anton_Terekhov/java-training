USE epamcoffee;

CREATE USER IF NOT EXISTS 'app_demo'@'localhost'
  IDENTIFIED BY 'demo';
CREATE USER IF NOT EXISTS 'app_user'@'localhost'
  IDENTIFIED BY 'user';
CREATE USER IF NOT EXISTS 'app_manager'@'localhost'
  IDENTIFIED BY 'manager';
CREATE USER IF NOT EXISTS'app_admin'@'localhost'
  IDENTIFIED BY 'admin';

/*
CREATE ROLE IF NOT EXISTS 'app_admin'; 
CREATE ROLE IF NOT EXISTS 'app_manager';
CREATE ROLE IF NOT EXISTS  'app_user';

GRANT 'app_admin' TO 'app_admin'@'localhost';
GRANT 'app_manager' TO 'app_manager'@'localhost';
GRANT 'app_user' TO 'app_user'@'localhost';
GRANT 'app_user' TO 'app_demo'@'localhost';
*/

GRANT ALL ON epamcoffee.appuser TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.appuserrole TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.dish TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.dishtype TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.menucard TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.userorder TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.userorderitem TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.orderitemstatus TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.ordertype TO 'app_admin'@'localhost';
GRANT ALL ON epamcoffee.userorder TO 'app_admin'@'localhost';

GRANT SELECT ON epamcoffee.dish TO 'app_manager'@'localhost';
GRANT SELECT ON epamcoffee.dishtype TO 'app_manager'@'localhost';
GRANT SELECT ON epamcoffee.menucard TO 'app_manager'@'localhost';
GRANT INSERT, UPDATE ON epamcoffee.userorder TO 'app_manager'@'localhost';

GRANT SELECT ON epamcoffee.dish TO 'app_user'@'localhost';
GRANT SELECT ON epamcoffee.dishtype TO 'app_user'@'localhost';
GRANT SELECT ON epamcoffee.menucard TO 'app_user'@'localhost';

GRANT SELECT ON epamcoffee.dish TO 'app_demo'@'localhost';
GRANT SELECT ON epamcoffee.dishtype TO 'app_demo'@'localhost';
GRANT SELECT ON epamcoffee.menucard TO 'app_demo'@'localhost';
