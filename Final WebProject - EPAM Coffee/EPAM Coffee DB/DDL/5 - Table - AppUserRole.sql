USE epamcoffee;

DROP TABLE IF EXISTS AppUserRole;
CREATE TABLE AppUserRole
(
	UserRoleId INT(6) AUTO_INCREMENT PRIMARY KEY,
    RoleNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    RoleNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
    RoleDescriptionRus VARCHAR(500) CHARACTER SET utf8 NOT NULL,
    RoleDescriptionEng VARCHAR(500) CHARACTER SET utf8 NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL
);

INSERT INTO AppUserRole (RoleNameRus, RoleNameEng, RoleDescriptionRus, RoleDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Администратор', 'Administrator', 'Главный администратор приложения', 'Main administrator of application', NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUserRole (RoleNameRus, RoleNameEng, RoleDescriptionRus, RoleDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Управлящий', 'Manager', 'Управляет заказами', 'Manage orders', NOW(), 'Admin', NULL, NULL);
INSERT INTO AppUserRole (RoleNameRus, RoleNameEng, RoleDescriptionRus, RoleDescriptionEng, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Пользователь', 'User', 'Рядовой пользователь приложения', 'Regual application user', NOW(), 'Admin', NULL, NULL);