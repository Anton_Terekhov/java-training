USE epamcoffee;

DROP TABLE IF EXISTS DishType;
CREATE TABLE DishType 
(
	DishTypeId INT(6) AUTO_INCREMENT PRIMARY KEY,
	DishTypeNameRus VARCHAR(50) CHARACTER SET utf8 NOT NULL,
	DishTypeNameEng VARCHAR(50) CHARACTER SET utf8 NOT NULL,
	DishTypeSortOrder INT(6) NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL
);

INSERT INTO DishType (DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Суп', 'Soup', 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO DishType (DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Салат', 'Salad', 2, NOW(), 'Admin', NULL, NULL);
INSERT INTO DishType (DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Гарнир', 'Fixings', 3, NOW(), 'Admin', NULL, NULL);
INSERT INTO DishType (DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Горячее', 'Hot meal', 4, NOW(), 'Admin', NULL, NULL);
INSERT INTO DishType (DishTypeNameRus, DishTypeNameEng, DishTypeSortOrder, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES ('Десерт', 'Dessert', 5, NOW(), 'Admin', NULL, NULL);