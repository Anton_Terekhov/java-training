USE epamcoffee;

DROP TABLE IF EXISTS UserOrderItem;
CREATE TABLE UserOrderItem
(
	UserOrderItemId INT(6) AUTO_INCREMENT PRIMARY KEY,
    OrderId INT(6) NOT NULL,
    MenuCardId INT(6) NOT NULL,
    Amount DECIMAL(6,2) NOT NULL,
    OrderItemStatusId INT(6) NOT NULL,
	InsertDT TIMESTAMP NOT NULL,
    InsertBy VARCHAR(30) CHARACTER SET utf8 NOT NULL,
	UpdateDT TIMESTAMP NULL,
    UpdateBy VARCHAR(30) CHARACTER SET utf8 NULL,
    CONSTRAINT FK_UserOrderItem_UserOrder FOREIGN KEY (OrderId)
    REFERENCES UserOrder(OrderId),
    CONSTRAINT FK_UserOrderItem_MenuCard FOREIGN KEY (MenuCardId)
    REFERENCES MenuCard(MenuCardId),
    CONSTRAINT FK_UserOrderItem_OrderItemStatus FOREIGN KEY (OrderItemStatusId)
    REFERENCES OrderItemStatus(OrderItemStatusId)
);

/*2017-05-01*/
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (1, 1, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (1, 8, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (1, 5, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (1, 11, 1, 1, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (2, 2, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (2, 6, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (2, 10, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (2, 12, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (2, 16, 1, 1, NOW(), 'Admin', NULL, NULL);
    
    
/*2017-05-02*/
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (3, 17, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (3, 19, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (3, 21, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (3, 26, 1, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (4, 17, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (4, 20, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (4, 23, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (4, 24, 1, 1, NOW(), 'Admin', NULL, NULL);
    
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (5, 18, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (5, 22, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (5, 25, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (5, 27, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (5, 31, 1, 1, NOW(), 'Admin', NULL, NULL);
    
    
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (6, 18, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (6, 19, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (6, 20, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (6, 21, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (6, 25, 1, 1, NOW(), 'Admin', NULL, NULL);

INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (7, 18, 1, 1, NOW(), 'Admin', NULL, NULL);
INSERT INTO UserOrderItem (OrderId, MenuCardId, Amount, OrderItemStatusId, InsertDT, InsertBy, UpdateDT, UpdateBy)
	VALUES (7, 20, 1, 1, NOW(), 'Admin', NULL, NULL);