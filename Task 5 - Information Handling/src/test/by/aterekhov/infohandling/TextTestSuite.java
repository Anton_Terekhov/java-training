package test.by.aterekhov.infohandling;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.by.aterekhov.infohandling.text.CompositeToolTest;

@Suite.SuiteClasses({CompositeToolTest.class})
@RunWith(Suite.class)
public class TextTestSuite {
}
