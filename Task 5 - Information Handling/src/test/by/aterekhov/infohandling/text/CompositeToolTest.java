package test.by.aterekhov.infohandling.text;

import by.aterekhov.infohandling.iooperation.InputText;
import by.aterekhov.infohandling.runner.ProjectConstant;
import by.aterekhov.infohandling.text.CompositeTool;
import by.aterekhov.infohandling.textaction.TextAction;
import by.aterekhov.infohandling.texthandler.MainTextHandler;
import org.junit.Assert;
import org.junit.Test;

public class CompositeToolTest {

    @Test
    public void toStringTest()
    {
        MainTextHandler handler = new MainTextHandler();
        CompositeTool compositeText;

        //read text from file
        StringBuilder sb = InputText.inputTextFromFile(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        //parse text
        compositeText = handler.handleText(sb.toString());

        //compare
        Assert.assertEquals(sb.toString(), compositeText.toString());
    }
}
