package by.aterekhov.infohandling.textaction;

import by.aterekhov.infohandling.text.CompositeTool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class TextAction {

    private final static Logger logger = LogManager.getLogger(TextAction.class);

    public static void printAllWordsByAlphabet(CompositeTool wholeText)
    {

        ArrayList<String> wordInText = wholeText.returnWord();
        wordInText.sort((p1, p2) -> p1.toLowerCase().compareTo(p2.toLowerCase()));

        logger.info("--- Information handling - Task 4 ---");
        logger.info("All text elements (digits + words) by alphabet. Each row - new first letter/digit.");
        logger.info("-------------------------------------");
        logger.debug(wordInText);
        int i = 0;
        int j;
        while (i<wordInText.size())
        {
            String logRow = wordInText.get(i).substring(0,1).toUpperCase() + ": " + wordInText.get(i);
            j = i+1;

            while (j<wordInText.size())
            {

                if(wordInText.get(i).substring(0,1).toUpperCase().equals(wordInText.get(j).substring(0,1).toUpperCase()))
                {
                    logRow += ", " + wordInText.get(j);
                }
                else
                {
                    i=j-1;
                    break;
                }
                j++;
            }
            i++;
            logger.info(logRow);
            logger.info("-------------------------------------");

            //exit from loop when was found last letter/digit
            if (j==wordInText.size())
            {
                break;
            }
        }
    }

    public static void printWholeText(CompositeTool wholeText)
    {
        logger.info("-------------------------------------");
        logger.info("The whole text, that was returned from composite, is presented below.");
        logger.info("-------------------------------------");
        logger.info(wholeText.toString());
        logger.info("-------------------------------------");
    }
}
