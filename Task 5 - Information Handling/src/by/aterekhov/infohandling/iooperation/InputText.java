package by.aterekhov.infohandling.iooperation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class InputText {
    private final static Logger logger = LogManager.getLogger(InputText.class);

    public static StringBuilder inputTextFromFile(String filePath, String fileName)
    {

        BufferedReader br = null;
        StringBuilder sb = null;

        try {
            FileReader fr = new FileReader(filePath + fileName);
            br = new BufferedReader(fr);
            sb = new StringBuilder();
            String line = br.readLine();

            while (line != null)
            {
                sb.append("    ");
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            logger.warn("File is not found.");
            logger.warn("Details:\n" + e.toString());
        } catch (IOException e) {
            logger.warn("Wrong file read/write operation.");
            logger.warn("Details:\n" + e.toString());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                logger.warn("File can't be closed.");
                logger.warn("Details:\n" + ex.toString());
            }
        }

        return sb;
    }
}
