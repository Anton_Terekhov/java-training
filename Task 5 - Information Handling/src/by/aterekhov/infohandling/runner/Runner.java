package by.aterekhov.infohandling.runner;

import by.aterekhov.infohandling.iooperation.InputText;
import by.aterekhov.infohandling.text.CompositeTool;
import by.aterekhov.infohandling.textaction.TextAction;
import by.aterekhov.infohandling.texthandler.*;

public class Runner {

    public static void main(String[] args)
    {
        MainTextHandler handler = new MainTextHandler();
        CompositeTool compositeText;

        //read text from file
        StringBuilder sb = InputText.inputTextFromFile(ProjectConstant.FILE_PATH, ProjectConstant.FILE_NAME);

        //parse text
        compositeText = handler.handleText(sb.toString());

        //return detailed information about composite
        compositeText.returnTextPieceAndDetails();

        //print whole text
        TextAction.printWholeText(compositeText);

        //task 4 - print all elements by alphabet, each line - new first letter/digit
        TextAction.printAllWordsByAlphabet(compositeText);

    }
}
