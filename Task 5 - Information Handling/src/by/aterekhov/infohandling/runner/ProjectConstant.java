package by.aterekhov.infohandling.runner;

public class ProjectConstant
{
    //Regular expressions
    public static final String PARAGRAPH_REGEXP = "\\s{4}.*[.!?]\n";
    public static final String SENTENCE_REGEXP = "[^.!?]*[.!?]";
    public static final String ELEMENT_REGEXP = "(\\w+)|([,.!?:]+)|(\\s+)|([']+)";
    public static final String MARK_REGEXP = "[,.!?:]+";
    public static final String SPACE_REGEXP = "\\s+";
    public static final String APOSTROPHE_REGEXP = "[']+";

    //File info
    public static final String FILE_PATH = "data/";
    public static final String FILE_NAME = "text.txt";

    private ProjectConstant() {};
}
