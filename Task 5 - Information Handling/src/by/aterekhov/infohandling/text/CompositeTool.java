package by.aterekhov.infohandling.text;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class CompositeTool implements ComponentImpl {

    private final static Logger logger = LogManager.getLogger(CompositeTool.class);
    public enum CompositeType {TEXT, PARAGRAPH, SENTENCE };
    private CompositeType compositeType;
    private ArrayList<ComponentImpl> text;

    public CompositeTool(String compositeName)
    {
        compositeType = CompositeType.valueOf(compositeName.toUpperCase());
        text = new ArrayList<ComponentImpl>();
    }

    @Override
    public void add(ComponentImpl textPiece)
    {
        text.add(textPiece);
    }

    @Override
    public void remove(ComponentImpl textPiece)
    {
        text.remove(textPiece);
    }

    @Override
    public int countElements()
    {

        int count = 0;
        for(ComponentImpl textPiece : text) {
            count+=textPiece.countElements();
        }
        return count;
    }

    @Override
    public String returnTextPieceAndDetails()
    {
        String result = "";

        for(ComponentImpl textPiece : text) {
            result += textPiece.returnTextPieceAndDetails();
        }
        logger.info("-----------------------------");
        logger.info("\tText piece is '" + compositeType.name().toLowerCase() + "', size tool: " + text.size()
                + ", number of elements: " + countElements() );
        logger.info(result);
        logger.info("-----------------------------");
        return result;
    }

    @Override
    public ArrayList<String> returnWord()
    {
        ArrayList<String> result = new ArrayList<String>();

        for(ComponentImpl textPiece : text) {
            result.addAll(textPiece.returnWord());
        }
        return result;
    }

    @Override
    public String returnTextType()
    {
        return compositeType.name().toLowerCase();
    }

    @Override
    public String toString()
    {
        String result = "";
        for(ComponentImpl textPiece : text) {
            result += textPiece.toString();
            if (textPiece.returnTextType().equals("paragraph"))
            {
                result += '\n';
            }
        }
        return result;
    }

}


