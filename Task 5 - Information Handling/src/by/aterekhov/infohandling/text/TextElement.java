package by.aterekhov.infohandling.text;

import by.aterekhov.infohandling.runner.ProjectConstant;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextElement implements ComponentImpl {

    public enum TextElementType {WORD, MARK, SPACE, APOSTROPHE};

    private String element;
    private TextElementType textElementType;

    public TextElement(String element)
    {
        this.element = element;

        Pattern patternMark = Pattern.compile(ProjectConstant.MARK_REGEXP);
        Matcher matcherMark = patternMark.matcher(element);
        Pattern patternSpace = Pattern.compile(ProjectConstant.SPACE_REGEXP);
        Matcher matcherSpace = patternSpace.matcher(element);
        Pattern patternApostrophe = Pattern.compile(ProjectConstant.APOSTROPHE_REGEXP);
        Matcher matcherApostrophe = patternApostrophe.matcher(element);

        if (matcherMark.find())
        {
            textElementType = TextElementType.valueOf("MARK");
        }
        else if (matcherSpace.find())
        {
            textElementType = TextElementType.valueOf("SPACE");
        }
        else if (matcherApostrophe.find())
        {
            textElementType = TextElementType.valueOf("APOSTROPHE");
        }
        else
        {
            textElementType = TextElementType.valueOf("WORD");
        }
    }

    @Override
    public String returnTextPieceAndDetails()
    {
        return element;
    }

    @Override
    public ArrayList<String> returnWord()
    {
        ArrayList<String> result = new ArrayList<String>();
        if (textElementType.name().equals("WORD"))
        {
            result.add(element);
        }
        return result;
    }

    @Override
    public int countElements()
    {
        return 1;
    }

    @Override
    public String returnTextType()
    {
        return textElementType.name();
    }

    @Override
    public void add(ComponentImpl c) {}

    @Override
    public void remove(ComponentImpl c) {}

    @Override
    public String toString()
    {
        return element;
    }
}