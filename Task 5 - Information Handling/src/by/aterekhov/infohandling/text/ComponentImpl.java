package by.aterekhov.infohandling.text;

import java.util.ArrayList;

public interface ComponentImpl
{
    String toString();
    String returnTextPieceAndDetails();
    ArrayList<String> returnWord();
    String returnTextType();
    void add(ComponentImpl c);
    void remove(ComponentImpl c);
    int countElements();
}
