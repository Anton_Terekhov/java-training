package by.aterekhov.infohandling.texthandler;

import by.aterekhov.infohandling.runner.ProjectConstant;
import by.aterekhov.infohandling.text.CompositeTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphHandling implements TextHandlerImpl {

    private final static Logger logger = LogManager.getLogger(ParagraphHandling.class);
    private TextHandlerImpl chain;

    @Override
    public void setNextChain(TextHandlerImpl nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void handleText(CompositeTool paragraph, String text)
    {
        Pattern pattern = Pattern.compile(ProjectConstant.SENTENCE_REGEXP);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find())
        {
            logger.info("-----------------------------");
            logger.info("----- Parser - Sentence -----");
            logger.info("-----------------------------");
            logger.info("The application found a sentence during text parsing, details are below.");
            logger.info("Sentence: '" + matcher.group() + "'");
            CompositeTool sentence = new CompositeTool("Sentence");
            paragraph.add(sentence);
            this.chain.handleText(sentence, matcher.group());
        }
    }
}
