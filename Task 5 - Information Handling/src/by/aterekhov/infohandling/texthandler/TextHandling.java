package by.aterekhov.infohandling.texthandler;

import by.aterekhov.infohandling.runner.ProjectConstant;
import by.aterekhov.infohandling.text.CompositeTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextHandling implements TextHandlerImpl {

    private final static Logger logger = LogManager.getLogger(TextHandling.class);
    private TextHandlerImpl chain;

    @Override
    public void setNextChain(TextHandlerImpl nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void handleText(CompositeTool wholeText, String text)
    {
        logger.info("-----------------------------");
        logger.info("---- Parser - Input Text ----");
        logger.info("-----------------------------");
        logger.info("The application will parse text that is presented below.");
        logger.info("Text:\n" + text);

        Pattern pattern = Pattern.compile(ProjectConstant.PARAGRAPH_REGEXP);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find())
        {
            logger.info("-----------------------------");
            logger.info("-----Parser - Paragraph -----");
            logger.info("-----------------------------");
            logger.info("The application found a paragraph during text parsing, details are below.");
            logger.info("Paragraph: '" + matcher.group() + "'");
            String str = matcher.group();
            CompositeTool paragraph = new CompositeTool("Paragraph");
            wholeText.add(paragraph);
            this.chain.handleText(paragraph, str);
        }

    }
}
