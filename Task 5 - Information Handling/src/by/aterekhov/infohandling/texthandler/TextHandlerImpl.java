package by.aterekhov.infohandling.texthandler;

import by.aterekhov.infohandling.text.CompositeTool;

public interface TextHandlerImpl {
    void handleText(CompositeTool wholeText, String text);
    void setNextChain(TextHandlerImpl nextChain);
}
