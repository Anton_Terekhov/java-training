package by.aterekhov.infohandling.texthandler;

import by.aterekhov.infohandling.runner.ProjectConstant;
import by.aterekhov.infohandling.text.CompositeTool;
import by.aterekhov.infohandling.text.TextElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceHandling implements TextHandlerImpl {

    private final static Logger logger = LogManager.getLogger(SentenceHandling.class);
    private TextHandlerImpl chain;

    @Override
    public void setNextChain(TextHandlerImpl nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void handleText(CompositeTool sentence, String text)
    {
        Pattern pattern = Pattern.compile(ProjectConstant.ELEMENT_REGEXP);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find())
        {
            logger.info("-----------------------------");
            logger.info("--- Parser - Text Element ---");
            logger.info("-----------------------------");
            logger.info("The application found a text element during text parsing, details are below.");
            logger.info("Element: '" + matcher.group() + "'");
            TextElement element = new TextElement(matcher.group());
            logger.info("Element type: " +element.returnTextType().toLowerCase());
            sentence.add(element);
        }
    }
}
