package by.aterekhov.infohandling.texthandler;

import by.aterekhov.infohandling.text.CompositeTool;

public class MainTextHandler {

    private TextHandlerImpl wholeText;

    public MainTextHandler()
    {
        // initialize the chain
        this.wholeText = new TextHandling();
        TextHandlerImpl paragraph = new ParagraphHandling();
        TextHandlerImpl sentence = new SentenceHandling();

        // set the chain of responsibility
        wholeText.setNextChain(paragraph);
        paragraph.setNextChain(sentence);
    }

    public CompositeTool handleText(String str)
    {
        CompositeTool compositeText = new CompositeTool("Text");
        wholeText.handleText(compositeText, str);
        return compositeText;
    }
}
