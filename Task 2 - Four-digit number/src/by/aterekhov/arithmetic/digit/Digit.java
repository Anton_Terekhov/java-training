package by.aterekhov.arithmetic.digit;

public class Digit {

    private int someNumber;

    public void setNumber(int inNumber)
    {
        someNumber = inNumber;
    }

    public int getNumber()
    {
        return someNumber;
    }

}
