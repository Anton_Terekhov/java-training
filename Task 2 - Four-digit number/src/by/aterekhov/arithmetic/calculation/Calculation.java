package by.aterekhov.arithmetic.calculation;


import by.aterekhov.arithmetic.digit.Digit;

public class Calculation {

    public static int calculateDigitMultiply(Digit digit)
    {
        int currentNumber = Math.abs(digit.getNumber());
        int multiplyResult = 1;

        do
        {
            multiplyResult *= currentNumber%10;
            currentNumber=currentNumber/10;
        } while (currentNumber>0);

        return multiplyResult;
    }
}
