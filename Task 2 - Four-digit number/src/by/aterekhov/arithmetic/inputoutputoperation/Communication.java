package by.aterekhov.arithmetic.inputoutputoperation;

import by.aterekhov.arithmetic.calculation.Calculation;
import by.aterekhov.arithmetic.creator.DigitCreator;
import by.aterekhov.arithmetic.digit.Digit;

import java.util.Scanner;

public class Communication {
    private Scanner scanner = new Scanner(System.in);

    public void printMainDialog()
    {
        int inNumber;

        do {
            System.out.println("Please, input 4 digit number.");

            //input of some digit
            if (this.scanner.hasNextInt()) {
                inNumber = scanner.nextInt();

                Digit digit;

                try
                {
                    digit = DigitCreator.createDigit(inNumber);
                    Calculation.calculateDigitMultiply(digit);
                    System.out.println("Number: " + digit.getNumber() + ", multiply result: " + Calculation.calculateDigitMultiply(digit) + ".\n");
                }
                catch (Exception e)
                {
                    System.out.println("Warning: there are wrong number. Should be range 1000-9999");
                }

            }
            //not digit
            else
            {
                System.out.println("Wrong number. Please, try again.");
                scanner.next();
            }
        } while(true);
    }

}
