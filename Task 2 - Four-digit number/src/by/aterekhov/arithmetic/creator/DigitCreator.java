package by.aterekhov.arithmetic.creator;

import by.aterekhov.arithmetic.digit.Digit;
import by.aterekhov.arithmetic.validator.Validator;

public class DigitCreator {

    public static Digit createDigit(int inNumber)
    {

        try
        {
            if (Validator.checkNumber(inNumber))
            {
                Digit digit = new Digit();
                digit.setNumber(inNumber);
                return digit;
            }
        }
        catch (NullPointerException e)
        {
            System.out.println("Digit can't be created. Input number: " + inNumber);
        }

        return null;

    };
}
