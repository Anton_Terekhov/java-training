package by.aterekhov.arithmetic.validator;

public class Validator {

    public static boolean checkNumber(int someNumber)
    {
        return (Math.abs(someNumber)>=1000 && Math.abs(someNumber)<=9999);
    }
}
