package test.by.aterekhov.arithmetic.creator;

import by.aterekhov.arithmetic.creator.DigitCreator;
import by.aterekhov.arithmetic.digit.Digit;
import org.junit.Assert;
import org.junit.Test;

public class DigitCreatorTest {

    @Test
    public void checkFourDigitValue()
    {
        Digit digit;
        digit = DigitCreator.createDigit(1454);
        int actual = digit.getNumber();
        int expected = 1454;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkTwoDigitValue()
    {
        Digit digit;
        digit = DigitCreator.createDigit(48);
        Assert.assertNull(digit);
    }

    @Test(timeout = 35)
    public void isNumberTestTime()
    {
        Digit digit;
        for (int i = 1000; i<9999; i++)
        {
            digit = DigitCreator.createDigit(i);
        }
    }
}
