package test.by.aterekhov.arithmetic.calculation;

import by.aterekhov.arithmetic.calculation.Calculation;
import by.aterekhov.arithmetic.creator.DigitCreator;
import by.aterekhov.arithmetic.digit.Digit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculationTest {
    private int inNumber;
    private int outMultiplyResult;

    public CalculationTest(int inNumber, int outMultiplyResult)
    {
        this.inNumber = inNumber;
        this.outMultiplyResult = outMultiplyResult;
    }

    @Parameters
    public static Collection<Object[]> numberExamples() {
        return Arrays.asList(new Object[][]{
                {1234, 24},
                {1111, 1},
                {9999, 6561},
                {4726, 336}
        });
    }

    @Test
    public void checkCalcValues()
    {
        Digit digit;
        digit = DigitCreator.createDigit(this.inNumber);
        int expected = this.outMultiplyResult;
        int actual = Calculation.calculateDigitMultiply(digit);
        Assert.assertEquals(expected, actual);

    }
}
