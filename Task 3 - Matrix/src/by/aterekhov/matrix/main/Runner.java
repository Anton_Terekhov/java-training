package by.aterekhov.matrix.main;

public class Runner
{
    public static void main(String[] args){
        int n = 8;

        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++){
            System.out.print("[");
            for (int j = 0; j < n; j++){
                if (i == 0 || i == (n - 1) || j == 0 || j == (n - 1)){
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = 0;
                }
                System.out.printf("%4d", matrix[i][j]);
            }
            System.out.println("   ]");
        }

    }
}
